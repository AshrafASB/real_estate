@include('website.layout.header')
<!-- banner -->
<div class="inside-banner">
    <div class="container">
        <span class="pull-right"><a href="{{route('welcome')}}">Home</a> / Register</span>
        <h2>Register</h2>
    </div>
</div>
<!-- banner -->


<div class="container">
    <div class="spacer">
        <div class="row register">
            <div class="col-lg-6 col-lg-offset-3 col-sm-6 col-sm-offset-3 col-xs-12 ">
                <form class="" role="form" method="POST" action="{{ route('register') }}">
                    @csrf
                    @method('post')
                {{--  Start Name  --}}
                <input type="text" class="form-control" placeholder="Full Name" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                @error('name')
                <span class="invalid-feedback @error('name') is-invalid @enderror" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                {{--  End Name  --}}

                {{--  Start Email  --}}
                <input type="text" class="form-control @error('email') is-invalid @enderror" placeholder="Enter Email" name="email" value="{{ old('email') }}" required autocomplete="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                {{--  End Email  --}}

                {{--  Start Password  --}}
                <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                {{--  End Password  --}}

                {{--  Start Confirm Password  --}}
                <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" required autocomplete="new-password">
                @error('password_confirmation')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                {{--  End Confirm Password  --}}

                {{--  Start Confirm Address  --}}
                <textarea rows="6" class="form-control" placeholder="Address" name="address"></textarea>
                @error('address')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                {{--  End Confirm Address  --}}

                <button type="submit" class="btn btn-success" name="Submit">Register</button>

                </form>
                <br>
                -------------------------------------------------- If You have Account --------------------------------------------------
                <br><br>
                <a class="btn btn-success" href="{{route('login')}}"> Login </a>
            </div>

        </div>
    </div>
</div>

@include('website.layout.footer')
