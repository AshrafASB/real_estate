@include('website.layout.header')
<!-- banner -->
<div class="inside-banner">
    <div class="container">
        <span class="pull-right"><a href="{{route('welcome')}}">Home</a> / Login</span>
        <h2>Login</h2>
    </div>
</div>
<!-- banner -->


<div class="container">
    <div class="spacer">
        <div class="row register">
            <div class="col-lg-6 col-lg-offset-3 col-sm-6 col-sm-offset-3 col-xs-12 ">

                <form class="" role="form" method="POST" action="{{ route('login') }}">
                    @csrf
                    @method('post')
                <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Enter Email" value="{{ old('email') }}"  autofocus>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

                <input type="password" name="password" class="form-control  @error('password') is-invalid @enderror" placeholder="Password"  autocomplete="current-password" >
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

                <button type="submit" class="btn btn-success" name="Submit">Login</button>

                </form>


            </div>

        </div>
    </div>
</div>

@include('website.layout.footer')




{{--<div class="limiter">--}}
{{--    <div class="container-login100" style="background-image: url('{{asset('login_page/images/img-01.jpg')}}');">--}}
{{--        <div class="wrap-login100 p-t-190 p-b-30">--}}
{{--            <form class="login100-form validate-form" method="POST" action="{{ route('login') }}">--}}
{{--                @csrf--}}

{{--                <div class="login100-form-avatar">--}}
{{--                    <img src="{{asset('dashboard_files/images/logo.png')}}" style="background-color: snow" alt="AVATAR">--}}
{{--                </div>--}}

{{--                <span class="login100-form-title p-t-20 p-b-45">--}}
{{--						Login - {{__('site.title_login')}}--}}
{{--					</span>--}}

{{--                <div class="wrap-input100 validate-input m-b-10" data-validate = "Username is required">--}}
{{--                    <input class="input100 @error('email') is-invalid @enderror" type="text" name="email" placeholder="email" value="{{ old('email') }}" autocomplete="email" autofocus>--}}
{{--                    @error('email')--}}
{{--                    <span class="invalid-feedback" role="alert">--}}
{{--								  <strong>{{ $message }}</strong>--}}
{{--							  </span>--}}
{{--                    @enderror--}}
{{--                    <span class="focus-input100"></span>--}}
{{--                    <span class="symbol-input100">--}}
{{--							<i class="fa fa-user"></i>--}}
{{--						</span>--}}
{{--                </div>--}}

{{--                <div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">--}}
{{--                    <input class="input100 @error('password') is-invalid @enderror" type="password" name="password" placeholder="Password" autocomplete="current-password" type="password" placeholder="Password">--}}
{{--						<span class="focus-input100"></span>--}}
{{--                    <span class="symbol-input100">--}}
{{--							<i class="fa fa-lock"></i>--}}
{{--						</span>--}}
{{--                </div>--}}

{{--                <div class="container-login100-form-btn p-t-10">--}}
{{--                    <button class="login100-form-btn">--}}
{{--                        Login--}}
{{--                    </button>--}}
{{--                </div>--}}

{{--            </form>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}



