@php

    $name_site = setting('name_site');
    $mission = setting('mission');
    $vision = setting('vision');
    $description = setting('description');
    $copyright = setting('copyright');

    $facebook = setting('facebook_link');
    $instagram = setting('instagram_link');
    $twitter = setting('twitter_link');
    $whatsUp = setting('whatsUp_link');
    $address = setting('address_link');
    $email = setting('email_link');
    $Company_Profile = setting('Company_Profile');

@endphp


@include('website.layout.header')
<!-- banner -->
<div class="inside-banner">
    <div class="container">
        <span class="pull-right"><a href="{{route('welcome')}}">Home</a> / Buy</span>
        <h2>Buy</h2>
    </div>
</div>
<!-- banner -->


<div class="container">
    <div class="spacer">
        <div class="row contact">
            <div class="col-lg-12" style="margin: 5% 20%">
                <img  style="margin: 2px 15%" src="{{asset('landing_real/images/logo.png')}}"  alt="">
            </div>
            <h3  style="margin: 2px 30%" >We are Receive your reservation wait us to call you </h3><br/>
            <a class="btn btn-info" style="margin: 2px 40%" href="{{route('welcome')}}">Back to Home Page </a>
        </div>
    </div>
</div>

@include('website.layout.footer')
