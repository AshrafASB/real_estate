@php

    $name_site = setting('name_site');
    $mission = setting('mission');
    $vision = setting('vision');
    $description = setting('description');
    $copyright = setting('copyright');

    $facebook = setting('facebook_link');
    $instagram = setting('instagram_link');
    $twitter = setting('twitter_link');
    $whatsUp = setting('whatsUp_link');
    $address = setting('address_link');
    $email = setting('email_link');
    $Company_Profile = setting('Company_Profile');

@endphp


@include('website.layout.header')
<!-- banner -->
<div class="inside-banner">
    <div class="container">
        <span class="pull-right"><a href="{{route('welcome')}}">Home</a> / Contact Us</span>
        <h2>Contact Us</h2>
    </div>
</div>
<!-- banner -->


<div class="container">
    <div class="spacer">
        <div class="row contact">
            <div class="col-lg-12" style="margin: 5% 20%">
                <img src="{{asset('landing_real/images/thank_you.png')}}"  alt="">
            </div>
                <a class="btn btn-info" style="margin: 2px 40%" href="{{route('welcome')}}">Home Page </a>
        </div>
    </div>
</div>

@include('website.layout.footer')
