@include('website.layout.header')
<!-- banner -->
<div class="inside-banner">
    <div class="container">
        <span class="pull-right"><a href="{{route('welcome')}}">Home</a> / Buy, Sale & Rent</span>
        <h2>Buy, Sale & Rent</h2>
    </div>
</div>
<!-- banner -->


<div class="container">
    <div class="properties-listing spacer">

        <div class="row">
            <div class="col-lg-3 col-sm-4 ">

                <div class="hot-properties hidden-xs">
                    <h4>Hot Properties</h4>

                    @foreach( $vip_real_estates as $vip_real_estate )
                        <div class="row">
                            <div class="col-lg-4 col-sm-5">
                                <img src="{{$vip_real_estate->image == null ?asset('landing_real/images/slider/3.jpg') :asset('storage/'.$vip_real_estate->image) }}" class="img-responsive img-circle" alt="properties"/>
                            </div>
                            <div class="col-lg-8 col-sm-7">
                                <h5><a href="{{route('reservation_vip',$vip_real_estate->id)}}">{{$vip_real_estate->name}}</a></h5>
                                <p class="price">${{$vip_real_estate->price}}</p>
                            </div>
                        </div>
                    @endforeach
                </div>


            </div>

            <div class="col-lg-9 col-sm-8">
                <div class="sortby clearfix">
                    <div class="pull-left result">Showing: all real estate </div>
{{--                    <div class="pull-right">--}}
{{--                        <select class="form-control">--}}
{{--                            <option>Sort by</option>--}}
{{--                            <option>Price: Low to High</option>--}}
{{--                            <option>Price: High to Low</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
                </div>


                <div class="row">

                   @foreach( $real_estates as $real_estate )
                   @if($real_estate->confirm == 1)
                    <!-- properties -->
                    <div class="col-lg-4 col-sm-6">
                        <div class="properties">

                            <div class="image-holder">
                                @if($real_estate->image == null)
                                    <img src="{{asset('landing_real/images/slider/5.jpg')}}" class="img-responsive" alt="properties">
                                @else
                                    <img src="{{asset('storage/'.$real_estate->image)}}" class="img-responsive" alt="properties">
                                @endif
                                <div class="status sold">Sold</div>
                            </div>

                            <h4><a href="{{route('property_detail',$real_estate->id)}}">{{$real_estate->name}}</a></h4>
                            <p class="price">Price: ${{$real_estate->price}}</p>
                            <div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room">{{$real_estate->bed_room_number}}</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">{{$real_estate->living_room_number}}</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Bathroom">{{$real_estate->parking_number}}</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">{{$real_estate->kitchen_number}}</span> </div>
                            <a class="btn btn-primary" href="{{route('property_detail',$real_estate->id)}}">View Details</a>
                        </div>
                    </div>
                    <!-- properties -->
                   @endif
                   @endforeach


                    <div class="center">
                        <ul class="pagination">
                            {{$real_estates->appends(request()->query())->links()}}

                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@include('website.layout.footer')
