@php

    $name_site = setting('name_site');
    $mission = setting('mission');
    $vision = setting('vision');
    $description = setting('description');
    $copyright = setting('copyright');

    $facebook = setting('facebook_link');
    $instagram = setting('instagram_link');
    $twitter = setting('twitter_link');
    $whatsUp = setting('whatsUp_link');
    $address = setting('address_link');
    $email = setting('email_link');
    $Company_Profile = setting('Company_Profile');

@endphp


@include('website.layout.header')
<!-- banner -->
<div class="inside-banner">
    <div class="container">
        <span class="pull-right"><a href="#">Home</a> / About Us</span>
        <h2>About Us</h2>
    </div>
</div>
<!-- banner -->


<div class="container">
    <div class="spacer">
        <div class="row">
            <div class="col-lg-8  col-lg-offset-2">
                <img src="{{asset('landing_real/images/about.jpg')}}" class="img-responsive thumbnail"  alt="realestate">
                <h3>Business Background</h3>
                <p> {{$description}}  </p>
                <h3>Company Profile</h3>
                <p>{{$Company_Profile}}</p>
            </div>

        </div>
    </div>
</div>

@include('website.layout.footer')
