@php

    $name_site = setting('name_site');
    $mission = setting('mission');
    $vision = setting('vision');
    $description = setting('description');
    $copyright = setting('copyright');

    $facebook = setting('facebook_link');
    $instagram = setting('instagram_link');
    $twitter = setting('twitter_link');
    $whatsUp = setting('whatsUp_link');
    $address = setting('address_link');
    $email = setting('email_link');
    $Company_Profile = setting('Company_Profile');

@endphp


@include('website.layout.header')
<!-- banner -->
<div class="inside-banner">
    <div class="container">
        <span class="pull-right"><a href="{{route('welcome')}}">Home</a> / Contact Us</span>
        <h2>Contact Us</h2>
    </div>
</div>
<!-- banner -->


<div class="container">
    <div class="spacer">
        <div class="row contact">
            <div class="col-lg-6 col-sm-6 ">
                <form action="{{route('storeMessage')}}" method="post">
                    @csrf
                    @method('post')
                    <input type="text" name="name" class="form-control" placeholder="Full Name">
                    <input type="text" name="email"  class="form-control" placeholder="Email">
                    <input type="text" name="phone_number"  class="form-control" placeholder="Contact Number">
                    <textarea rows="6" name="messages" class="form-control" placeholder="Message"></textarea>
                    <button type="submit" class="btn btn-success">Send Message</button>
                </form>
            </div>
            <div class="col-lg-6 col-sm-6 ">
                <div class="well">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6801.465245357791!2d34.46311034490093!3d31.53150280300259!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14fd7efd22360c41%3A0x8f23779889ea7aee!2z2KzYp9mF2LnYqSDYp9mE2YLYr9izINin2YTZhdmB2KrZiNit2KkgLSDZgdix2Lkg2LrYstip!5e0!3m2!1sar!2s!4v1621092874414!5m2!1sar!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>
            </div>
        </div>
    </div>
</div>

@include('website.layout.footer')
