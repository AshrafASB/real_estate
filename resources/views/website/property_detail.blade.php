@php

    $name_site = setting('name_site');
    $mission = setting('mission');
    $vision = setting('vision');
    $description = setting('description');
    $copyright = setting('copyright');

    $facebook = setting('facebook_link');
    $instagram = setting('instagram_link');
    $twitter = setting('twitter_link');
    $whatsUp = setting('whatsUp_link');
    $address = setting('address_link');
    $email = setting('email_link');

@endphp


@include('website.layout.header')
<!-- banner -->
<div class="inside-banner">
    <div class="container">
        <span class="pull-right"><a href="{{route('welcome')}}">Home</a> / Buy</span>
        <h2>Buy</h2>
    </div>
</div>
<!-- banner -->


<div class="container">
    <div class="properties-listing spacer">

        <div class="row">
            <div class="col-lg-3 col-sm-4 hidden-xs">
{{--                    VIP Real-estate           --}}
                <div class="hot-properties hidden-xs">
                    <h4>Hot Properties</h4>
                    @foreach( $vip_real_estates as $vip_real_estate )
                        <div class="row">
                            <div class="col-lg-4 col-sm-5">
                                <img src="{{$vip_real_estate->image == null ?asset('landing_real/images/slider/3.jpg') :asset('storage/'.$vip_real_estate->image) }}" class="img-responsive img-circle" alt="properties"/>
                            </div>
                            <div class="col-lg-8 col-sm-7">
                                <h5><a href="{{route('reservation_vip',$vip_real_estate->id)}}">{{$vip_real_estate->name}}</a></h5>
                                <p class="price">${{$vip_real_estate->price}}</p>
                            </div>
                        </div>
                    @endforeach

                </div>


                Advertisements
{{--                <div class="advertisement">--}}
{{--                    <h4>Advertisements</h4>--}}
{{--                    <img src="images/advertisements.jpg" class="img-responsive" alt="advertisement">--}}
{{--                </div>--}}
            </div>

            <div class="col-lg-9 col-sm-8 ">

                <h2>{{$property_detail->name}}</h2>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="property-images">
                            <!-- Slider Starts -->
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators hidden-xs">
                                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                                    <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                                    <li data-target="#myCarousel" data-slide-to="3" class=""></li>
                                </ol>
                                <div class="carousel-inner">
                                    <!-- Item 1 -->
                                    @if($property_detail->image == null)
                                        <div class="item active">
                                            <img src="{{asset('landing_real/images/slider/5.jpg')}}" class="properties" alt="properties" />
                                        </div>
                                    @else
                                        <div class="item active">
                                            <img src="{{asset('storage/'.$property_detail->image)}}" class="properties" alt="properties" />
                                        </div>
                                     @endif
                                    <!-- #Item 1 -->

                                    <!-- Item 2 -->
                                    @if($property_detail->image2 == null)
                                        <div class="item">
                                            <img src="{{asset('landing_real/images/slider/5.jpg')}}" class="properties" alt="properties" />
                                        </div>
                                    @else
                                        <div class="item">
                                            <img src="{{asset('storage/'.$property_detail->image2)}}" class="properties" alt="properties" />
                                        </div>
                                    @endif
                                    <!-- #Item 2 -->

                                    <!-- Item 3 -->
                                    @if($property_detail->image3 == null)
                                        <div class="item">
                                            <img src="{{asset('landing_real/images/slider/5.jpg')}}" class="properties" alt="properties" />
                                        </div>
                                    @else
                                        <div class="item">
                                            <img src="{{asset('storage/'.$property_detail->image3)}}" class="properties" alt="properties" />
                                        </div>
                                    @endif
                                    <!-- #Item 3 -->

                                    <!-- Item 4 -->
                                    @if($property_detail->image4 == null)
                                        <div class="item">
                                            <img src="{{asset('landing_real/images/slider/5.jpg')}}" class="properties" alt="properties" />
                                        </div>
                                    @else
                                        <div class="item">
                                            <img src="{{asset('storage/'.$property_detail->image4)}}" class="properties" alt="properties" />
                                        </div>
                                    @endif
                                    <!-- # Item 4 -->
                                </div>
                                <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                                <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                            </div>
                            <!-- #Slider Ends -->

                        </div>




                        <div class="spacer"><h4><span class="glyphicon glyphicon-th-list"></span> Properties Detail</h4>
                            <p>
                                {{$property_detail->details}}
                            </p>


                        </div>

                    {{-- map --}}
                        <div>
                            <h4><span class="glyphicon glyphicon-map-marker"></span> Location</h4>
                            <div class="well">
{{--                                <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Pulchowk,+Patan,+Central+Region,+Nepal&amp;aq=0&amp;oq=pulch&amp;sll=37.0625,-95.677068&amp;sspn=39.371738,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=Pulchowk,+Patan+Dhoka,+Patan,+Bagmati,+Central+Region,+Nepal&amp;ll=27.678236,85.316853&amp;spn=0.001347,0.002642&amp;t=m&amp;z=14&amp;output=embed">--}}
{{--                                </iframe>--}}
                                <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6801.465245357791!2d34.46311034490093!3d31.53150280300259!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14fd7efd22360c41%3A0x8f23779889ea7aee!2z2KzYp9mF2LnYqSDYp9mE2YLYr9izINin2YTZhdmB2KrZiNit2KkgLSDZgdix2Lkg2LrYstip!5e0!3m2!1sar!2s!4v1621092874414!5m2!1sar!2s"></iframe>
                            </div>

                        </div>

                    </div>
                    <div class="col-lg-4">
                        <div class="col-lg-12  col-sm-6">
                            <div class="property-info">
                                <p class="price">$ {{$property_detail->price}}</p>
                                <p class="area"><span class="glyphicon glyphicon-map-marker"></span> {{$property_detail->location}}</p>

{{--                                <div class="profile">--}}
{{--                                    <span class="glyphicon glyphicon-user"></span> Agent Details--}}
{{--                                    <p>John Parker<br>009 229 2929</p>--}}
{{--                                </div>--}}
                            </div>

                            <h6><span class="glyphicon glyphicon-home"></span> Availabilty</h6>
                            <div class="listing-detail">
                                <span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room">{{$property_detail->bed_room_number}}</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">{{$property_detail->living_room_number}}</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Bathroom">{{$property_detail->parking_number}}</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">{{$property_detail->kitchen_number}}</span> </div>

                        </div>
                        <div class="col-lg-12 col-sm-6 ">
                            <div class="enquiry">
                                <h6><span class="glyphicon glyphicon-envelope"></span> contact us for these real-estate</h6>
                                <form role="form" method="post" action="{{route('buy_real_estate')}}">
                                    @csrf
                                    @method('post')
                                    <input type="text" name="buyer_name" class="form-control" placeholder="Full Name"/>
                                    <input type="text" name="national_id" class="form-control" placeholder="Enter Your National id"/>
                                    <input type="text" name="email" class="form-control" placeholder="Enter Your Email"/>
                                    <input type="text" name="phone" class="form-control" placeholder="your Number"/>
                                    <input type="text" name="address" class="form-control" placeholder="your Address"/>
                                    <textarea rows="6" name="details" class="form-control" placeholder="Whats on your mind?"></textarea>
                                    <input type="hidden" name="user_id" class="form-control" value="{{ $property_detail->user_id }}" />
                                    <input type="hidden" name="real_estate_id" class="form-control" value="{{ $property_detail->id }}" />
                                    <button type="submit" class="btn btn-primary">Send Message</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('website.layout.footer')
