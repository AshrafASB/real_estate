@php

    $name_site = setting('name_site');
    $mission = setting('mission');
    $vision = setting('vision');
    $description = setting('description');
    $copyright = setting('copyright');

    $facebook = setting('facebook_link');
    $instagram = setting('instagram_link');
    $twitter = setting('twitter_link');
    $whatsUp = setting('whatsUp_link');
    $address = setting('address_link');
    $email = setting('email_link');

@endphp



    <!DOCTYPE html>
<html lang="en">
<head>
    <title>{{$name_site}} </title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="stylesheet" href="{{asset('landing_real/assets/bootstrap/css/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{asset('landing_real/assets/style.css')}}"/>
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="{{asset('landing_real/assets/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('landing_real/assets/script.js')}}"></script>



    <!-- Owl stylesheet -->
    <link rel="stylesheet" href="{{asset('landing_real/assets/owl-carousel/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('landing_real/assets/owl-carousel/owl.theme.css')}}">
    <script src="{{asset('landing_real/assets/owl-carousel/owl.carousel.js')}}"></script>
    <!-- Owl stylesheet -->


    <!-- slitslider -->
    <link rel="stylesheet" type="text/css" href="{{asset('landing_real/assets/slitslider/css/style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('landing_real/assets/slitslider/css/custom.css')}}" />
    <script type="text/javascript" src="{{asset('landing_real/assets/slitslider/js/modernizr.custom.79639.js')}}"></script>
    <script type="text/javascript" src="{{asset('landing_real/assets/slitslider/js/jquery.ba-cond.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('landing_real/assets/slitslider/js/jquery.slitslider.js')}}"></script>
    <!-- slitslider -->

</head>

<body>


<!-- Header Starts -->
<div class="navbar-wrapper">

    <div class="navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header">


                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>


            <!-- Nav Starts -->
            <div class="navbar-collapse  collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="{{route('welcome')}}">Home</a></li>
                    <li><a href="{{route('about')}}">About</a></li>
                    <li><a href="{{route('contact')}}">Contact</a></li>
                    @if(!auth()->user())
                        <li><a href="{{route('login')}}">Login</a></li>
                        <li><a href="{{route('register')}}">Register</a></li>
                    @else
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out fa-lg"></i>   {{ __('Logout') }}
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </a>
                        </li>
                        <li><a href="{{route('dashboard.sales_reservations.index')}}">Dashboard</a></li>
                    @endif
                </ul>
            </div>
            <!-- #Nav Ends -->

        </div>
    </div>

</div>
<!-- #Header Starts -->





<div class="container">

    <!-- Header Starts -->
    <div class="header">
        <a href="index.php"><img src="{{asset('landing_real/images/logo.png')}}" alt="Realestate"></a>

        <ul class="pull-right">
            <li><a data-toggle="modal" data-target="#buy_real_estate">Sale</a></li>
            <li><a href="{{route('all_real_estate')}}">Buy</a></li>
        </ul>
    </div>
    <!-- #Header Starts -->
</div>
