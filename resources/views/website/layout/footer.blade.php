@php

    $name_site = setting('name_site');
    $mission = setting('mission');
    $vision = setting('vision');
    $description = setting('description');
    $copyright = setting('copyright');

    $facebook = setting('facebook_link');
    $instagram = setting('instagram_link');
    $twitter = setting('twitter_link');
    $whatsUp = setting('whatsUp_link');
    $address = setting('address_link');
    $email = setting('email_link');

@endphp




<div class="footer">

    <div class="container">



        <div class="row">
            <div class="col-lg-3 col-sm-3">
                <h4>Information</h4>
                <ul class="row">
                    <li class="col-lg-12 col-sm-12 col-xs-3"><a href="{{route('about')}}">About</a></li>
                    <li class="col-lg-12 col-sm-12 col-xs-3"><a href="{{route('contact')}}">Contact</a></li>
                </ul>
            </div>



            <div class="col-lg-3 col-sm-3">
                <h4>Follow us</h4>
                <a href="{{$facebook}}"><img src="{{asset('landing_real/images/facebook.png')}}" alt="facebook"></a>
                <a href="{{$twitter}}"><img src="{{asset('landing_real/images/twitter.png')}}" alt="twitter"></a>
{{--                <a href="#"><img src="{{asset('landing_real/images/linkedin.png')}}" alt="linkedin"></a>--}}
                <a href="{{$instagram}}"><img src="{{asset('landing_real/images/instagram.png')}}" alt="instagram"></a>
            </div>

            <div class="col-lg-3 col-sm-3">
                <h4>Contact us</h4>
                <p><b>{{$name_site}}</b><br>
                    <span class="glyphicon glyphicon-map-marker"></span> {{$address}} <br>
                    <span class="glyphicon glyphicon-envelope"></span> {{$email}}<br>
                    <span class="glyphicon glyphicon-earphone"></span> {{$whatsUp}}</p>
            </div>
        </div>
        <p class="copyright">{{$copyright}}</p>


    </div></div>




<!-- Modal 1  -->
<div id="loginpop" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="row">
                <div class="col-sm-6 login">
                    <h4>Login</h4>
                    <form class="" role="form" method="POST" action="{{ route('login') }}">
                        @csrf
                        @method('post')
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputEmail2">Email address</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email" autofocus>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputPassword2">Password</label>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword2" placeholder="Password">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Remember me
                            </label>
                        </div>
                        <button type="submit" class="btn btn-success">Sign in</button>
                    </form>
                </div>
                <div class="col-sm-6">
                    <h4>New User Sign Up</h4>
                    <p>Join today and get updated with all the properties deal happening around.</p>
                    <button type="submit" class="btn btn-info"  onclick="window.location.href='register.php'">Join Now</button>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- Modal 1  -->
<div id="buy_real_estate" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="row">
                <div class="col-sm-6">
                    <h4>You must join us to add your Product</h4>
                    <p>make your Account from This Button.</p>
                    <button type="submit" class="btn btn-info"  onclick="window.location.href='{{route('register')}}'">Join Now</button>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- /.modal -->



</body>
</html>



