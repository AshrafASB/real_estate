@php

    $name_site = setting('name_site');
    $mission = setting('mission');
    $vision = setting('vision');
    $description = setting('description');
    $copyright = setting('copyright');

    $facebook = setting('facebook_link');
    $instagram = setting('instagram_link');
    $twitter = setting('twitter_link');
    $whatsUp = setting('whatsUp_link');
    $address = setting('address_link');
    $email = setting('email_link');

@endphp


@include('website.layout.header')
<div class="">
    <div id="slider" class="sl-slider-wrapper">

        <div class="sl-slider">
        @foreach( $vip_real_estates as $vip_real_estate )
            <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                <div class="sl-slide-inner">
                    <div class="bg-img" style="background-image: url({{$vip_real_estate->image == null ?asset('landing_real/images/slider/3.jpg') :asset('storage/'.$vip_real_estate->image) }});"></div>
                    <h2><a href="#">{{$vip_real_estate->name}}</a></h2>
                    <blockquote>
                        <p class="location"><span class="glyphicon glyphicon-map-marker"></span> {{$vip_real_estate->location}}</p>
                        <p>{{$vip_real_estate->details}}</p>
                        <cite>
                            <a href="{{route('reservation_vip',$vip_real_estate->id)}}" style="color: black"> $ {{$vip_real_estate->price}}</a>
                        </cite>
                    </blockquote>
                </div>
            </div>
        @endforeach

        </div><!-- /sl-slider -->



        <nav id="nav-dots" class="nav-dots">
            <span class="nav-dot-current"></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </nav>

    </div><!-- /slider-wrapper -->
</div>



<div class="banner-search">
    <div class="container">
        <!-- banner -->
        <h3>Join us</h3>
        <div class="searchbar">
            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-6 ">
                    <p>Join now and get updated with all the properties deals.</p>
                    <button class="btn btn-info"   data-toggle="modal" data-target="#loginpop">Login</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- banner -->
<div class="container">
    <div class="properties-listing spacer"> <a href="{{route('all_real_estate')}}" class="pull-right viewall">View All Listing</a>
        <h2>Featured Properties</h2>
        <div id="owl-example" class="owl-carousel">


            @foreach( $real_estates as $real_estate )
                @if($real_estate->confirm == 1)
                <div class="properties">

                        @if($real_estate->image == null)
                            <div class="image-holder">
                                <img src="{{asset('landing_real/images/slider/5.jpg')}}" class="img-responsive" alt="properties"/>
                            <div class="status sold">Sold</div>
                        @else
                            <div class="image-holder">
                                <img src="{{asset('storage/'.$real_estate->image)}}" class="img-responsive" alt="properties"/>
                            <div class="status sold">Sold</div>
                        @endif

                    </div>
                    <h4><a href="{{route('property_detail',$real_estate->id)}}">{{$real_estate->name}}</a></h4>
                    <p class="price">Price: ${{$real_estate->price}}</p>
                    <div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room">{{$real_estate->bed_room_number}}</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">{{$real_estate->living_room_number}}</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Bathroom">{{$real_estate->parking_number}}</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">{{$real_estate->kitchen_number}}</span> </div>
                    <a class="btn btn-primary" href="{{route('property_detail',$real_estate->id)}}">View Details</a>
                </div>
                @endif
            @endforeach



        </div>
    </div>
    <div class="spacer">
        <div class="row">
            <div class="col-lg-12 col-sm-9 recent-view">
                <h3>About Us</h3>
                <p>{{ $description }}<br><a href="#">Learn More</a></p>
            </div>
        </div>
    </div>
</div>
@include('website.layout.footer')
