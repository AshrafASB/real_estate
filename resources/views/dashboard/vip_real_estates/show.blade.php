@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($vip_real_estate)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Show_real_estates')}}
                    </i>
               </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                      {{__('site.Add_real_estates') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.real_estates.index')}}">{{__('site._real_estates')}}</a></li>
            @if(isset($vip_real_estate))
                <li class="breadcrumb-item">{{__('site.Update_real_estates')}}</li>
            @else
                <li class="breadcrumb-item">{{__('site.Add_real_estates')}}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
             <form action="#" method="post" enctype="multipart/form-data">
                 @csrf

                 @include('dashboard.partials._errors')

                 <div class="form-group">
                     <label>{{__('site.name')}} :</label>
                     <input disabled type="text" name="name" class="form-control" value="{{isset($vip_real_estate)?$vip_real_estate->name:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.details')}} :</label>
                     <textarea name="details" disabled cols="30" rows="10"  class="form-control">{{isset($vip_real_estate)?$vip_real_estate->details:""}}</textarea>
                 </div>
                 <div class="form-group">
                     <label>{{__('site.location')}} :</label>
                     <input disabled type="text" name="location" class="form-control" value="{{isset($vip_real_estate)?$vip_real_estate->location:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.price')}} by Dollars $ :</label>
                     <input disabled type="number" name="price" class="form-control" value="{{isset($vip_real_estate)?$vip_real_estate->price:""}}">
                 </div>

                 @if(auth()->user()->hasRole('super_admin'))
                     {{-- Select of users --}}
                      <div class="form-group">
                          <label>{{__('site.users')}} :</label>
                          <select name="user_id" class="form-control" disabled>
                             @foreach( $users as $user )
                                  <option value="{{$user->id}}" {{isset($vip_real_estate)?($user->id == $vip_real_estate->user_id?'selected':'' ):''}}> {{ $user->name }}</option>
                             @endforeach
                          </select>
                      </div>
                 @else
                     <input disabled type="hidden" name="users" class="form-control" value="{{auth()->user()->id}}">
                 @endif


                 {{-- Select of  --}}
                  <div class="form-group">
                      <label>{{__('site.categories')}} :</label>
                      <select name="category_id" class="form-control" disabled>
                         @foreach( $categories as $category )
                              <option value="{{$category->id}}" {{isset($vip_real_estate)?($category->id == $vip_real_estate->category_id?'selected':'' ):''}}> {{ $category->category_name }}</option>
                         @endforeach
                      </select>
                  </div>

                 <div class="form-group">
                     <label>{{__('site.image')}} :</label>
                     @isset($vip_real_estate)
                         @if($vip_real_estate->image == null)
                             <br/><img class="imageSize" src="{{asset('landing_real/images/logo.png')}}" alt="">
                         @else
                             <br/><img class="imageSize" src="{{asset('storage/'.$vip_real_estate->image)}}" alt="">
                             <input disabled type="file" name="image" class="form-control" value="{{isset($vip_real_estate)?$vip_real_estate->image:""}}">
                         @endif
                     @else
                         <input disabled type="file" name="image" class="form-control" value="{{isset($vip_real_estate)?$vip_real_estate->image:""}}">
                     @endisset
                 </div>

                 <div class="form-group">

                     <a class="btn btn-primary" href="{{route('dashboard.vip_real_estates.index')}}">
                         <i class="fa fa-backward"></i>
                         {{__('site.back')}}
                     </a>
                 </div>
             </form>

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection
