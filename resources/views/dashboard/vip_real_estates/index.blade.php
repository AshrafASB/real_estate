@extends('layouts.dashboard.app')

@section('content')
    <style>
        .dropbtn {
            background-color: #04AA6D;
            color: white;
            padding: 16px;
            font-size: 16px;
            border: none;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f1f1f1;
            min-width: 90px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
        }

        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }
        .dropdown-content a:hover {background-color: #ddd;}

        .dropdown-content .b1 {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        .dropdown-content b1:hover {background-color: #ddd;}

        .dropdown:hover .dropdown-content {display: block;}

        .dropdown:hover .dropbtn {background-color: #3e8e41;}



    </style>

    <div class="app-title">
        <div>
            <h1><i class="fa fa-list"></i> {{__('site.vip_real_estates')}} </h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{ __('site.Dashboard')}} </a></li>
            <li class="breadcrumb-item"> {{__('site.vip_real_estates')}}</li>
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
                {{-- this form for Search button                --}}
                <form action="" >
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="search" autofocus class="form-control" placeholder="Search" value="{{request()->search}}">
                            </div>
                        </div>{{-- end-of-col-4 --}}

                        <div class="col-md-4">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>Search</button>
                                    @if(auth()->user()->hasPermission('create_real_estates'))
                                        <a href="{{route('dashboard.vip_real_estates.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                                    @else
                                        <a href="#" disabled class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                                    @endif
                            </div>
                        </div>{{-- end-of-col-4 --}}


                    </div>{{-- end-of-row --}}
                </form>{{-- end-of-form --}}

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}

        @if(auth()->user()->hasRole('super_admin'))
            <div class="row">
                <div class="col-md-12">
                    <hr>
                    @if($vip_real_estates->count() > 0 )
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{__('site.name')}}</th>
                                <th>{{__('site.details')}}</th>
                                <th>{{__('site.location')}}</th>
                                <th width="12%">{{__('site.price')}}</th>
                                <th>{{__('site.users')}}</th>
                                <th>{{__('site.image')}}</th>
                                <th width="10%">{{__('site.action')}}</th>
                            </tr>
                            </thead>

                            @if(auth()->user()->hasRole('super_admin'))
                                <tbody>
                                @foreach($vip_real_estates as $index=>$vip_real_estate)
                                    <tr>
                                        <td>{{++$index}}</td>
                                        <td>{{$vip_real_estate->name}}</td>
                                        <td> {{\Illuminate\Support\Str::limit($vip_real_estate->details, 20)}} </td>
                                        <td>{{$vip_real_estate->location}}</td>
                                        <td>{{$vip_real_estate->price}} $</td>
                                        <td>{{$vip_real_estate->user->name}}</td>
                                        <td>
                                            @if($vip_real_estate->image == null)
                                                <img class="imageSize" src="{{asset('landing_real/images/logo.png')}}" alt="">
                                            @else
                                                <img class="imageSize" src="{{asset('storage/'.$vip_real_estate->image)}}" alt="">
                                            @endif
                                        </td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="dropbtn">Choices</button>
                                                <div class="dropdown-content">
                                                        {{--show buttom--}}
                                                        @if(auth()->user()->hasPermission('read_vip_real_estates'))
                                                            <a href="{{route('dashboard.vip_real_estates.show', $vip_real_estate->id)}}" class="btn btn-info btn-sm"><i class="fa fa-eye"> {{__('site.show')}}</i></a>
                                                        @else
                                                            <a href="#" disabled="" class="btn btn-info btn-sm"><i class="fa fa-eye">{{__('site.show')}}</i></a>
                                                        @endif

                                                        {{--Edit buttom--}}
                                                        @if(auth()->user()->hasPermission('update_vip_real_estates'))
                                                            <a href="{{route('dashboard.vip_real_estates.edit', $vip_real_estate->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-edit">{{__('site.Edit')}}</i></a>
                                                        @else
                                                            <a href="#" disabled="" class="btn btn-warning btn-sm"><i class="fa fa-edit">{{__('site.Edit')}}</i></a>
                                                        @endif

                                                        {{--Delete buttom--}}
                                                        @if(auth()->user()->hasPermission('delete_vip_real_estates'))
                                                            <form action="{{route('dashboard.vip_real_estates.destroy', $vip_real_estate->id)}}" method="post" style="display: inline-block">
                                                                @csrf
                                                                @method('delete')
                                                                <button type="submit" class="btn btn-danger btn-sm delete b1"><i class="fa fa-trash"></i>{{__('site.Delete')}}</button>
                                                            </form>
                                                        @else
                                                            <a href="#" disabled="" class="btn btn-danger btn-sm b1"><i class="fa fa-edit">{{__('site.Delete')}}</i></a>
                                                        @endif
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            @endif
                        </table>
                        {{$vip_real_estates->appends(request()->query())->links()}}
                    @else
                        <h3 style="font-weight: 400; text-align: center"> No Record Found</h3>
                    @endif
                </div>
            </div>
        @else


        @endif

    </div>{{--end-of-tile mb-4--}}


@endsection
