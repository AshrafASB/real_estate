@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($v_i_p_buy_reservation)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Update_v_i_p_buy_reservation')}}
                    </i>
               </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                      {{__('site.Add_v_i_p_buy_reservation') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.v_i_p_buy_reservation.index')}}">{{__('site.v_i_p_buy_reservation')}}</a></li>
            @if(isset($v_i_p_buy_reservation))
                <li class="breadcrumb-item">{{__('site.Update_v_i_p_buy_reservation')}}</li>
            @else
                <li class="breadcrumb-item">{{__('site.Add_v_i_p_buy_reservation')}}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
             <form action="{{isset($v_i_p_buy_reservation)?route('dashboard.v_i_p_buy_reservation.update',$v_i_p_buy_reservation->id):route('dashboard.v_i_p_buy_reservation.store')}}" method="post">
                 @csrf
                 @if(isset($v_i_p_buy_reservation))
                     @method('put')
                 @else
                     @method('post')
                 @endif

                 @include('dashboard.partials._errors')

                 <div class="form-group">
                     <label>{{__('site.buyer_name')}} :</label>
                     <input type="text" name="buyer_name" class="form-control" value="{{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->buyer_name:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.national_id')}} :</label>
                     <input type="text" name="national_id" class="form-control" value="{{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->national_id:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.email')}} :</label>
                     <input type="text" name="email" class="form-control" value="{{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->email:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.phone')}} :</label>
                     <input type="text" name="phone" class="form-control" value="{{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->phone:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.address')}} :</label>
                     <input type="text" name="address" class="form-control" value="{{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->address:""}}">
                 </div>

                 <div class="form-group">
                     <label>{{__('site.details')}} :</label>
                     <textarea name="details" cols="30" rows="10"  class="form-control">{{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->details:""}}</textarea>
                 </div>

                 @if(auth()->user()->hasRole('super_admin'))
                 {{-- Select of users --}}
                 <div class="form-group">
                     <label>{{__('site.users')}} :</label>
                     <select name="user_id" class="form-control">
                         @foreach( $users as $user )
                             <option value="{{$user->id}}" {{isset($v_i_p_buy_reservation)?($user->id == $v_i_p_buy_reservation->user_id?'selected':'' ):''}}> {{ $user->name }}</option>
                         @endforeach
                     </select>
                 </div>
                 @else
                     <input disabled type="hidden" name="user_id" class="form-control" value="{{auth()->user()->id}}">
                @endif

                 {{-- Select of  --}}
                 <div class="form-group">
                     <label>{{__('site.real_estate')}} :</label>
                     <select name="real_estate_id" class="form-control">
                         @foreach( $real_estates as $real_estate )
                             <option value="{{$real_estate->id}}" {{isset($v_i_p_buy_reservation)?($v_i_p_buy_reservation->real_estate_id == $real_estate->id?'selected':'' ):''}}> {{ $real_estate->name }}</option>
                         @endforeach
                     </select>
                 </div>


                 <div class="form-group">
                     <button type="submit" class="btn btn-primary">
                         @if( isset($v_i_p_buy_reservation) )
                             <i class="fa fa-edit"></i>
                             {{__('site.Update')}}
                         @else
                             <i class="fa fa-plus"></i>
                             {{__('site.Add')}}
                         @endif

                     </button>
                 </div>
             </form>

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection
