@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            <div>
                <h1><i class="fa fa-file-text-o"></i> Invoice for VIP Buy Reservation</h1>
                <p>{{ __('site.Details_VIP_buy_reservation')}}</p>
            </div>

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.v_i_p_buy_reservations.index')}}">{{__('site.v_i_p_buy_reservations')}}</a></li>
            @if(isset($v_i_p_buy_reservation))
                <li class="breadcrumb-item">{{__('site.Details_VIP_buy_reservation')}}</li>
            @else
                <li class="breadcrumb-item">{{__('site.Details_VIP_buy_reservation')}}</li>
            @endif
        </ul>
    </div>

                 <div class="tile">
                     <section class="invoice">
                         <div class="row mb-4">
                             <div class="col-6">
                                 <h2 class="page-header"><i class="fa fa-globe"></i> Vali Inc</h2>
                             </div>
                             <div class="col-5">
                                 <h5 class="text-right">Date: {{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->created_at:'no data'}} </h5>

                             </div>
                             <div class="col-1">
                             <a class="btn btn-primary" href="{{route('dashboard.v_i_p_buy_reservations.index')}}">
                                 <i class="fa fa-forward"></i>
                             </a>
                             </div>
                         </div>
                         <div class="row invoice-info">
                             <div class="col-4">From
                                 <address>Name: <strong>{{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->buyer_name:'no data'}}</strong><br>Address: <strong>{{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->address:'no data'}}</strong><br>Phone: <strong>{{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->phone:'no data'}}</strong><br>Email: <strong>{{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->email:'no data'}}</strong></address>
                             </div>
                             <div class="col-4">To
                                 <address><strong>{{isset($v_i_p_buy_reservation)?auth()->user()->name:'no data'}}</strong><br>since: {{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->created_at->diffForHumans():'no data'}}<br>Email: {{isset($v_i_p_buy_reservation)?auth()->user()->email:'no data'}}</address>
                             </div>
                             <div class="col-4"><b>Invoice #{{rand(0, 9999)}}</b><br><br><b>Order ID:</b> {{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->id:'no data'}}<br><b>Account:</b> {{rand(0, 999)}}-{{rand(0, 9999)}}<br><b>Real-Estate Name:</b> {{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->vip_real_estate->name:'no data'}}</div>
                         </div>
                         <hr>
                         <div class="row">
                             <div class="col-12 table-responsive">
                                 Buyer Messages:
                                 <p>
                                        {{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->details:'no data'}}
                                 </p>
                             </div>
                         </div>
                         <hr>
                         <div class="row">
                             <div class="col-12 table-responsive">
                                 Details of Product:
                                 <hr>
                                 <table class="table table-striped">
                                     <thead>
                                     <tr>
                                         <th>name</th>
                                         <th>location</th>
                                         <th>price #</th>
                                     </tr>
                                     </thead>
                                     <tbody>
                                     <tr>
                                         <td>{{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->vip_real_estate->name:'no data'}}</td>
                                         <td>{{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->vip_real_estate->location:'no data'}}</td>
                                         <td>{{isset($v_i_p_buy_reservation)?$v_i_p_buy_reservation->vip_real_estate->price:'no data'}}</td>
                                     </tr>
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                         <div class="row d-print-none mt-2">
                             <div class="col-12 text-right"><a class="btn btn-primary" onclick="window.print()" target="_blank"><i class="fa fa-print"></i> Print</a></div>
                         </div>

                     </section>

                 </div>

@endsection
