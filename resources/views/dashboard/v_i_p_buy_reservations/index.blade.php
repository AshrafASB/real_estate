@extends('layouts.dashboard.app')

@section('content')
    <style>
        .dropbtn {
            background-color: #04AA6D;
            color: white;
            padding: 16px;
            font-size: 16px;
            border: none;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f1f1f1;
            min-width: 90px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
        }

        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }
        .dropdown-content a:hover {background-color: #ddd;}

        .dropdown-content .b1 {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        .dropdown-content b1:hover {background-color: #ddd;}

        .dropdown:hover .dropdown-content {display: block;}

        .dropdown:hover .dropbtn {background-color: #3e8e41;}



    </style>
    <div class="app-title">
        <div>
            <h1><i class="fa fa-list"></i> {{__('site.v_i_p_buy_reservations')}} </h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">Dashboard</a></li>
            <li class="breadcrumb-item"> {{__('site.v_i_p_buy_reservations')}}</li>
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
                {{-- this form for Search button                --}}
                <form action="" >
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="search" autofocus class="form-control" placeholder="Search" value="{{request()->search}}">
                            </div>
                        </div>{{-- end-of-col-4 --}}


                        <div class="col-md-4">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>Search</button>
                                    @if(auth()->user()->hasPermission('create_v_i_p_buy_reservations'))
                                        <a href="{{route('dashboard.v_i_p_buy_reservations.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                                    @else
                                        <a href="#" disabled class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                                    @endif
                            </div>
                        </div>{{-- end-of-col-4 --}}


                    </div>{{-- end-of-row --}}
                </form>{{-- end-of-form --}}

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}

        @if(auth()->user()->hasRole('super_admin'))
        {{--  Admin user  --}}
            <div class="row">
            <div class="col-md-12">
                <hr>
                @if($v_i_p_buy_reservations->count() > 0 )
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('site.buyer_name')}}</th>
                            <th  width="15%">{{__('site.email')}}</th>
                            <th>{{__('site.phone')}}</th>
                            <th>{{__('site.vip_real_estate')}}</th>
                            <th width="15%">{{__('site.owner_user')}}</th>
                            <th >{{__('site.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($v_i_p_buy_reservations as $index=>$v_i_p_buy_reservation)
                            <tr>
                                <td>{{++$index}}</td>
                                <td>{{$v_i_p_buy_reservation->buyer_name}}</td>
                                <td>{{$v_i_p_buy_reservation->email}}</td>
                                <td>{{$v_i_p_buy_reservation->phone}}</td>
                                <td>{{$v_i_p_buy_reservation->vip_real_estate->name}}</td>
                                <td>{{$v_i_p_buy_reservation->user->name}}</td>
{{--                                <td> {{\Illuminate\Support\Str::limit($v_i_p_buy_reservation->details, 100)}} </td>--}}
                                <td>
                                    <div class="dropdown">
                                        <button class="dropbtn">Choices</button>
                                        <div class="dropdown-content">
                                    {{--show buttom--}}
                                    @if(auth()->user()->hasPermission('update_v_i_p_buy_reservations'))
                                        <a href="{{route('dashboard.v_i_p_buy_reservations.show', $v_i_p_buy_reservation->id)}}" class="btn btn-info btn-sm"><i class="fa fa-eye"> Show</i></a>
                                    @else
                                        <a href="#" disabled="" class="btn btn-info btn-sm"><i class="fa fa-eye">{{__('site.show')}}</i></a>
                                    @endif
                                    {{--Edit buttom--}}
                                    @if(auth()->user()->hasPermission('update_v_i_p_buy_reservations'))
                                        <a href="{{route('dashboard.v_i_p_buy_reservations.edit', $v_i_p_buy_reservation->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-edit"> Edit</i></a>
                                    @else
                                        <a href="#" disabled="" class="btn btn-warning btn-sm"><i class="fa fa-edit">{{__('site.Edit')}}</i></a>
                                    @endif

                                    {{--Delete buttom--}}
                                    @if(auth()->user()->hasPermission('delete_v_i_p_buy_reservations'))
                                        <form action="{{route('dashboard.v_i_p_buy_reservations.destroy', $v_i_p_buy_reservation->id)}}" method="post" style="display: inline-block">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger delete"><i class="fa fa-trash"></i>{{__('site.Delete')}}</button>
                                        </form>
                                    @else
                                        <a href="#" disabled="" class="btn btn-danger delete"><i class="fa fa-edit">{{__('site.Delete')}}</i></a>
                                    @endif
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>

                    </table>
                    {{$v_i_p_buy_reservations->appends(request()->query())->links()}}
                @else
                    <h3 style="font-weight: 400; text-align: center"> No Record Found</h3>
                @endif
            </div>
        </div>

        @endif



    </div>{{--end-of-tile mb-4--}}


@endsection
