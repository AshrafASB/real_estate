@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-list"></i> {{__('site.sales_reservations')}} </h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">Dashboard</a></li>
            <li class="breadcrumb-item"> {{__('site.sales_reservations')}}</li>
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
                {{-- this form for Search button                --}}
                <form action="" >
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="search" autofocus class="form-control" placeholder="Search" value="{{request()->search}}">
                            </div>
                        </div>{{-- end-of-col-4 --}}


                        <div class="col-md-4">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>Search</button>
                                    @if(auth()->user()->hasPermission('create_sales_reservations'))
                                        <a href="{{route('dashboard.sales_reservations.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                                    @else
                                        <a href="#" disabled class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                                    @endif
                            </div>
                        </div>{{-- end-of-col-4 --}}


                    </div>{{-- end-of-row --}}
                </form>{{-- end-of-form --}}

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}

        @if(auth()->user()->hasRole('super_admin'))
        {{--  Admin user  --}}
            <div class="row">
            <div class="col-md-12">
                <hr>
                @if($sales_reservations->count() > 0 )
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('site.buyer_name')}}</th>
                            <th>{{__('site.email')}}</th>
                            <th>{{__('site.phone')}}</th>
                            <th>{{__('site.real_estate')}}</th>
                            <th>{{__('site.owner_user')}}</th>
                            <th>{{__('site.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sales_reservations as $index=>$sales_reservation)
                            <tr>
                                <td>{{++$index}}</td>
                                <td>{{$sales_reservation->buyer_name}}</td>
                                <td>{{$sales_reservation->email}}</td>
                                <td>{{$sales_reservation->phone}}</td>
                                <td>{{$sales_reservation->real_estate->name}}</td>
                                <td>{{$sales_reservation->user->name}}</td>
{{--                                <td> {{\Illuminate\Support\Str::limit($sales_reservation->details, 100)}} </td>--}}
                                <td>
                                    {{--show buttom--}}
                                    @if(auth()->user()->hasPermission('update_sales_reservations'))
                                        <a href="{{route('dashboard.sales_reservations.show', $sales_reservation->id)}}" class="btn btn-info btn-sm"><i class="fa fa-eye"> Show</i></a>
                                    @else
                                        <a href="#" disabled="" class="btn btn-info btn-sm"><i class="fa fa-eye">{{__('site.show')}}</i></a>
                                    @endif
                                    {{--Edit buttom--}}
                                    @if(auth()->user()->hasPermission('update_sales_reservations'))
                                        <a href="{{route('dashboard.sales_reservations.edit', $sales_reservation->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-edit"> Edit</i></a>
                                    @else
                                        <a href="#" disabled="" class="btn btn-warning btn-sm"><i class="fa fa-edit">{{__('site.Edit')}}</i></a>
                                    @endif

                                    {{--Delete buttom--}}
                                    @if(auth()->user()->hasPermission('delete_sales_reservations'))
                                        <form action="{{route('dashboard.sales_reservations.destroy', $sales_reservation->id)}}" method="post" style="display: inline-block">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i>{{__('site.Delete')}}</button>
                                        </form>
                                    @else
                                        <a href="#" disabled="" class="btn btn-danger btn-sm"><i class="fa fa-edit">{{__('site.Delete')}}</i></a>
                                    @endif

                                </td>
                            </tr>
                        @endforeach

                        </tbody>

                    </table>
                    {{$sales_reservations->appends(request()->query())->links()}}
                @else
                    <h3 style="font-weight: 400; text-align: center"> No Record Found</h3>
                @endif
            </div>
        </div>


        @else

        {{--  normal user  --}}
            <div class="row">
            <div class="col-md-12">
                <hr>
                @if($sales_reservations->count() > 0 )
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('site.buyer_name')}}</th>
                            <th>{{__('site.email')}}</th>
                            <th>{{__('site.phone')}}</th>
                            <th>{{__('site.real_estate')}}</th>
                            <th>{{__('site.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sales_reservations as $index=>$sales_reservation)
                            @if(auth()->user()->id == $sales_reservation->user_id)
                                <tr>
                                <td>{{++$index}}</td>
                                <td>{{$sales_reservation->buyer_name}}</td>
                                <td>{{$sales_reservation->email}}</td>
                                <td>{{$sales_reservation->phone}}</td>
                                <td>{{$sales_reservation->real_estate->name}}</td>
                                <td>
                                    {{--show buttom--}}
                                    @if(auth()->user()->hasPermission('update_sales_reservations'))
                                        <a href="{{route('dashboard.sales_reservations.show', $sales_reservation->id)}}" class="btn btn-info btn-sm"><i class="fa fa-eye"> Show</i></a>
                                    @else
                                        <a href="#" disabled="" class="btn btn-info btn-sm"><i class="fa fa-eye">{{__('site.show')}}</i></a>
                                    @endif
                                    {{--Edit buttom--}}
                                    @if(auth()->user()->hasPermission('update_sales_reservations'))
                                        <a href="{{route('dashboard.sales_reservations.edit', $sales_reservation->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-edit">Edit</i></a>
                                    @else
                                        <a href="#" disabled="" class="btn btn-warning btn-sm"><i class="fa fa-edit">{{__('site.Edit')}}</i></a>
                                    @endif

                                    {{--Delete buttom--}}
                                    @if(auth()->user()->hasPermission('delete_sales_reservations'))
                                        <form action="{{route('dashboard.sales_reservations.destroy', $sales_reservation->id)}}" method="post" style="display: inline-block">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i>{{__('site.Delete')}}</button>
                                        </form>
                                    @else
                                        <a href="#" disabled="" class="btn btn-danger btn-sm"><i class="fa fa-edit">{{__('site.Delete')}}</i></a>
                                    @endif

                                </td>
                            </tr>
                            @endif
                        @endforeach

                        </tbody>

                    </table>
                    {{$sales_reservations->appends(request()->query())->links()}}
                @else
                    <h3 style="font-weight: 400; text-align: center"> No Record Found</h3>
                @endif
            </div>
        </div>
        @endif



    </div>{{--end-of-tile mb-4--}}


@endsection
