@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            <div>
                <h1><i class="fa fa-file-text-o"></i> Invoice for Buy Reservation</h1>
                <p>{{ __('site.Details_buy_reservation')}}</p>
            </div>

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.sales_reservations.index')}}">{{__('site.sales_reservations')}}</a></li>
            @if(isset($sales_reservation))
                <li class="breadcrumb-item">{{__('site.Details_buy_reservation')}}</li>
            @else
                <li class="breadcrumb-item">{{__('site.Details_buy_reservation')}}</li>
            @endif
        </ul>
    </div>

                 <div class="tile">
                     <section class="invoice">
                         <div class="row mb-4">
                             <div class="col-6">
                                 <h2 class="page-header"><i class="fa fa-globe"></i> Vali Inc</h2>
                             </div>
                             <div class="col-5">
                                 <h5 class="text-right">Date: {{isset($sales_reservation)?$sales_reservation->created_at:'no data'}} </h5>

                             </div>
                             <div class="col-1">
                             <a class="btn btn-primary" href="{{route('dashboard.sales_reservations.index')}}">
                                 <i class="fa fa-forward"></i>
                             </a>
                             </div>
                         </div>
                         <div class="row invoice-info">
                             <div class="col-4">From
                                 <address>Name: <strong>{{isset($sales_reservation)?$sales_reservation->buyer_name:'no data'}}</strong><br>Address: <strong>{{isset($sales_reservation)?$sales_reservation->address:'no data'}}</strong><br>Phone: <strong>{{isset($sales_reservation)?$sales_reservation->phone:'no data'}}</strong><br>Email: <strong>{{isset($sales_reservation)?$sales_reservation->email:'no data'}}</strong></address>
                             </div>
                             <div class="col-4">To
                                 <address><strong>{{isset($sales_reservation)?auth()->user()->name:'no data'}}</strong><br>since: {{isset($sales_reservation)?$sales_reservation->created_at->diffForHumans():'no data'}}<br>Email: {{isset($sales_reservation)?auth()->user()->email:'no data'}}</address>
                             </div>
                             <div class="col-4"><b>Invoice #{{rand(0, 9999)}}</b><br><br><b>Order ID:</b> {{isset($sales_reservation)?$sales_reservation->id:'no data'}}<br><b>Account:</b> {{rand(0, 999)}}-{{rand(0, 9999)}}<br><b>Real-Estate Name:</b> {{isset($sales_reservation)?$sales_reservation->real_estate->name:'no data'}}</div>
                         </div>
                         <hr>
                         <div class="row">
                             <div class="col-12 table-responsive">
                                 Buyer Messages:
                                 <p>
                                        {{isset($sales_reservation)?$sales_reservation->details:'no data'}}
                                 </p>
                             </div>
                         </div>
                         <hr>
                         <div class="row">
                             <div class="col-12 table-responsive">
                                 Details of Product:
                                 <hr>
                                 <table class="table table-striped">
                                     <thead>
                                     <tr>
                                         <th>name</th>
                                         <th>location</th>
                                         <th>price #</th>
                                         <th>Bedroom number</th>
                                         <th>living room number</th>
                                         <th>Bathroom number</th>
                                         <th>kitchen number</th>
                                     </tr>
                                     </thead>
                                     <tbody>
                                     <tr>
                                         <td>{{isset($sales_reservation)?$sales_reservation->real_estate->name:'no data'}}</td>
                                         <td>{{isset($sales_reservation)?$sales_reservation->real_estate->location:'no data'}}</td>
                                         <td>{{isset($sales_reservation)?$sales_reservation->real_estate->price:'no data'}}</td>
                                         <td>{{isset($sales_reservation)?$sales_reservation->real_estate->bed_room_number:'no data'}}</td>
                                         <td>{{isset($sales_reservation)?$sales_reservation->real_estate->living_room_number:'no data'}}</td>
                                         <td>{{isset($sales_reservation)?$sales_reservation->real_estate->parking_number:'no data'}}</td>
                                         <td>{{isset($sales_reservation)?$sales_reservation->real_estate->kitchen_number:'no data'}}</td>
                                     </tr>
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                         <div class="row d-print-none mt-2">
                             <div class="col-12 text-right"><a class="btn btn-primary" onclick="window.print()" target="_blank"><i class="fa fa-print"></i> Print</a></div>
                         </div>

                     </section>

                 </div>

@endsection
