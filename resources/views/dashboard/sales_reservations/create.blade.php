@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($sales_reservation)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Update_sales_reservations')}}
                    </i>
               </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                      {{__('site.Add_sales_reservations') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.sales_reservations.index')}}">{{__('site.sales_reservations')}}</a></li>
            @if(isset($sales_reservation))
                <li class="breadcrumb-item">{{__('site.Update_sales_reservations')}}</li>
            @else
                <li class="breadcrumb-item">{{__('site.Add_sales_reservations')}}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
             <form action="{{isset($sales_reservation)?route('dashboard.sales_reservations.update',$sales_reservation->id):route('dashboard.sales_reservations.store')}}" method="post">
                 @csrf
                 @if(isset($sales_reservation))
                     @method('put')
                 @else
                     @method('post')
                 @endif

                 @include('dashboard.partials._errors')

                 <div class="form-group">
                     <label>{{__('site.buyer_name')}} :</label>
                     <input type="text" name="buyer_name" class="form-control" value="{{isset($sales_reservation)?$sales_reservation->buyer_name:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.national_id')}} :</label>
                     <input type="text" name="national_id" class="form-control" value="{{isset($sales_reservation)?$sales_reservation->national_id:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.email')}} :</label>
                     <input type="text" name="email" class="form-control" value="{{isset($sales_reservation)?$sales_reservation->email:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.phone')}} :</label>
                     <input type="text" name="phone" class="form-control" value="{{isset($sales_reservation)?$sales_reservation->phone:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.address')}} :</label>
                     <input type="text" name="address" class="form-control" value="{{isset($sales_reservation)?$sales_reservation->address:""}}">
                 </div>

                 <div class="form-group">
                     <label>{{__('site.details')}} :</label>
                     <textarea name="details" cols="30" rows="10"  class="form-control">{{isset($sales_reservation)?$sales_reservation->details:""}}</textarea>
                 </div>

                 @if(auth()->user()->hasRole('super_admin'))
                 {{-- Select of users --}}
                 <div class="form-group">
                     <label>{{__('site.users')}} :</label>
                     <select name="user_id" class="form-control">
                         @foreach( $users as $user )
                             <option value="{{$user->id}}" {{isset($sales_reservation)?($user->id == $sales_reservation->user_id?'selected':'' ):''}}> {{ $user->name }}</option>
                         @endforeach
                     </select>
                 </div>
                 @else
                     <input disabled type="hidden" name="user_id" class="form-control" value="{{auth()->user()->id}}">
                @endif

                 {{-- Select of  --}}
                 <div class="form-group">
                     <label>{{__('site.real_estate')}} :</label>
                     <select name="real_estate_id" class="form-control">
                         @foreach( $real_estates as $real_estate )
                             <option value="{{$real_estate->id}}" {{isset($sales_reservation)?($sales_reservation->real_estate_id == $real_estate->id?'selected':'' ):''}}> {{ $real_estate->name }}</option>
                         @endforeach
                     </select>
                 </div>


                 <div class="form-group">
                     <button type="submit" class="btn btn-primary">
                         @if( isset($sales_reservation) )
                             <i class="fa fa-edit"></i>
                             {{__('site.Update')}}
                         @else
                             <i class="fa fa-plus"></i>
                             {{__('site.Add')}}
                         @endif

                     </button>
                 </div>
             </form>

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection
