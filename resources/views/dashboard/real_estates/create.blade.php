@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($real_estate)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Update_real_estates')}}
                    </i>
               </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                      {{__('site.Add_real_estates') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.real_estates.index')}}">{{__('site._real_estates')}}</a></li>
            @if(isset($real_estate))
                <li class="breadcrumb-item">{{__('site.Update_real_estates')}}</li>
            @else
                <li class="breadcrumb-item">{{__('site.Add_real_estates')}}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
             <form action="{{isset($real_estate)?route('dashboard.real_estates.update',$real_estate->id):route('dashboard.real_estates.store')}}" method="post" enctype="multipart/form-data">
                 @csrf
                 @if(isset($real_estate))
                     @method('put')
                 @else
                     @method('post')
                 @endif

                 @include('dashboard.partials._errors')

                 <div class="form-group">
                     <label>{{__('site.name')}} :</label>
                     <input type="text" name="name" class="form-control" value="{{isset($real_estate)?$real_estate->name:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.details')}} :</label>
                     <textarea name="details" cols="30" rows="10"  class="form-control">{{isset($real_estate)?$real_estate->details:""}}</textarea>
                 </div>
                 <div class="form-group">
                     <label>{{__('site.location')}} :</label>
                     <input type="text" name="location" class="form-control" value="{{isset($real_estate)?$real_estate->location:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.price')}} by Dollars $ :</label>
                     <input type="number" name="price" class="form-control" value="{{isset($real_estate)?$real_estate->price:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.bed_room_number')}} :</label>
                     <input type="number" name="bed_room_number" class="form-control" value="{{isset($real_estate)?$real_estate->bed_room_number:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.living_room_number')}} :</label>
                     <input type="number" name="living_room_number" class="form-control" value="{{isset($real_estate)?$real_estate->living_room_number:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.Bathroom_number')}} :</label>
                     <input type="number" name="parking_number" class="form-control" value="{{isset($real_estate)?$real_estate->parking_number:""}}">
                 </div>
                 <div class="form-group">
                     <label>{{__('site.kitchen_number')}} :</label>
                     <input type="number" name="kitchen_number" class="form-control" value="{{isset($real_estate)?$real_estate->kitchen_number:""}}">
                 </div>
                 <div class="form-group">
{{--                     <label>{{__('site.latitude')}} :</label>--}}
                     <input type="hidden" name="latitude" class="form-control" value="{{isset($real_estate)?$real_estate->latitude:""}}">
                 </div>
                 <div class="form-group">
{{--                     <label>{{__('site.Longitude')}} :</label>--}}
                     <input type="hidden" name="Longitude" class="form-control" value="{{isset($real_estate)?$real_estate->Longitude:""}}">
                 </div>

                 {{-- Select of users --}}
                 @if(auth()->user()->hasRole('super_admin'))
                  <div class="form-group">
                      <label>{{__('site.users')}} :</label>
                      <select name="user_id" class="form-control">
                         @foreach( $users as $user )
                              <option value="{{$user->id}}" {{isset($real_estate)?($user->id == $real_estate->user_id?'selected':'' ):''}}> {{ $user->name }}</option>
                         @endforeach
                      </select>
                  </div>
                 @else
                     <input disabled type="hidden" name="user_id" class="form-control" value="{{auth()->user()->id}}">
                 @endif


                 {{-- Select of  --}}
                  <div class="form-group">
                      <label>{{__('site.categories')}} :</label>
                      <select name="category_id" class="form-control">
                         @foreach( $categories as $category )
                              <option value="{{$category->id}}" {{isset($real_estate)?($category->id == $real_estate->category_id?'selected':'' ):''}}> {{ $category->category_name }}</option>
                         @endforeach
                      </select>
                  </div>
                 <div class="form-group">
                     <label>{{__('site.image')}} :</label>
                     @isset($real_estate)
                         @if($real_estate->image == null)
                             <br/><img class="imageSize" src="{{asset('landing_real/images/logo.png')}}" alt="">
                             <input type="file" name="image" class="form-control" value="{{isset($real_estate)?$real_estate->image:""}}">
                         @else
                             <br/><img class="imageSize" src="{{asset('storage/'.$real_estate->image)}}" alt="">
                             <input type="file" name="image" class="form-control" value="{{isset($real_estate)?$real_estate->image:""}}">
                         @endif
                     @else
                         <input type="file" name="image" class="form-control" value="{{isset($real_estate)?$real_estate->image:""}}">
                     @endisset
                 </div>
                 <div class="form-group">
                     <label>{{__('site.image2')}} :</label>
                     @isset($real_estate)
                         @if($real_estate->image2 == null)
                             <br/><img class="imageSize" src="{{asset('landing_real/images/logo.png')}}" alt="">
                             <input type="file" name="image2" class="form-control" value="{{isset($real_estate)?$real_estate->image:""}}">
                         @else
                             <br/><img class="imageSize" src="{{asset('storage/'.$real_estate->image2)}}" alt="">
                             <input type="file" name="image2" class="form-control" value="{{isset($real_estate)?$real_estate->image:""}}">
                         @endif
                     @else
                         <input type="file" name="image2" class="form-control" value="{{isset($real_estate)?$real_estate->image2:""}}">
                     @endisset
                 </div>
                 <div class="form-group">
                     <label>{{__('site.image3')}} :</label>
                     @isset($real_estate)
                         @if($real_estate->image3 == null)
                             <br/><img class="imageSize" src="{{asset('landing_real/images/logo.png')}}" alt="">
                             <input type="file" name="image3" class="form-control" value="{{isset($real_estate)?$real_estate->image:""}}">
                         @else
                             <br/><img class="imageSize" src="{{asset('storage/'.$real_estate->image3)}}" alt="">
                             <input type="file" name="image3" class="form-control" value="{{isset($real_estate)?$real_estate->image:""}}">
                         @endif
                     @else
                         <input type="file" name="image3" class="form-control" value="{{isset($real_estate)?$real_estate->image3:""}}">
                     @endisset
                 </div>
                 <div class="form-group">
                     <label>{{__('site.image4')}} :</label>
                     @isset($real_estate)
                         @if($real_estate->image4 == null)
                             <br/><img class="imageSize" src="{{asset('landing_real/images/logo.png')}}" alt="">
                             <input type="file" name="image4" class="form-control" value="{{isset($real_estate)?$real_estate->image:""}}">
                         @else
                             <br/><img class="imageSize" src="{{asset('storage/'.$real_estate->image4)}}" alt="">
                             <input type="file" name="image4" class="form-control" value="{{isset($real_estate)?$real_estate->image:""}}">
                         @endif
                     @else
                         <input type="file" name="image4" class="form-control" value="{{isset($real_estate)?$real_estate->image4:""}}">
                     @endisset
                 </div>

                 <div class="form-group">
                     <button type="submit" class="btn btn-primary">
                         @if( isset($real_estate) )
                             <i class="fa fa-edit"></i>
                             {{__('site.Update')}}
                         @else
                             <i class="fa fa-plus"></i>
                             {{__('site.Add')}}
                         @endif

                     </button>
                 </div>
             </form>

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection
