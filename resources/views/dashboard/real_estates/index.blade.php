@extends('layouts.dashboard.app')

@section('content')
    <style>
        .dropbtn {
            background-color: #04AA6D;
            color: white;
            padding: 16px;
            font-size: 16px;
            border: none;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f1f1f1;
            min-width: 90px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
        }

        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }
        .dropdown-content a:hover {background-color: #ddd;}

        .dropdown-content .b1 {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        .dropdown-content b1:hover {background-color: #ddd;}

        .dropdown:hover .dropdown-content {display: block;}

        .dropdown:hover .dropbtn {background-color: #3e8e41;}



    </style>
    <div class="app-title">
        <div>
            <h1><i class="fa fa-list"></i> {{__('site.real_estates')}} </h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{ __('site.Dashboard')}} </a></li>
            <li class="breadcrumb-item"> {{__('site.real_estates')}}</li>
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
                {{-- this form for Search button                --}}
                <form action="" >
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="search" autofocus class="form-control" placeholder="Search" value="{{request()->search}}">
                            </div>
                        </div>{{-- end-of-col-4 --}}

                        <div class="col-md-4">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>Search</button>
                                    @if(auth()->user()->hasPermission('create_real_estates'))
                                        <a href="{{route('dashboard.real_estates.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                                    @else
                                        <a href="#" disabled class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                                    @endif
                            </div>
                        </div>{{-- end-of-col-4 --}}


                    </div>{{-- end-of-row --}}
                </form>{{-- end-of-form --}}

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}

        @if(auth()->user()->hasRole('super_admin'))
            <div class="row">
                <div class="col-md-12">
                    <hr>
                    @if($real_estates->count() > 0 )
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{__('site.name')}}</th>
                                {{--                            <th>{{__('site.details')}}</th>--}}
                                <th>{{__('site.location')}}</th>
                                <th>{{__('site.users')}}</th>
                                <th>{{__('site.price')}}</th>
                                <th>{{__('site.image')}}</th>
                                @if(auth()->user()->hasRole('super_admin'))
                                    <th>{{__('site.confirm')}}</th>
                                @endif
                                <th>{{__('site.action')}}</th>
                            </tr>
                            </thead>

                            @if(auth()->user()->hasRole('super_admin'))
                                <tbody>
                                @foreach($real_estates as $index=>$real_estate)
                                    <tr>
                                        <td>{{++$index}}</td>
                                        <td>{{$real_estate->name}}</td>
                                        {{--                                <td> {{\Illuminate\Support\Str::limit($real_estate->details, 50)}} </td>--}}
                                        <td>{{$real_estate->location}}</td>
                                        <td>{{$real_estate->user->name}}</td>
                                        <td>{{$real_estate->price}} $</td>
                                        <td>
                                            @if($real_estate->image == null)
                                                <img class="imageSize" src="{{asset('landing_real/images/logo.png')}}" alt="">
                                            @else
                                                <img class="imageSize" src="{{asset('storage/'.$real_estate->image)}}" alt="">
                                            @endif
                                        </td>
                                        @if(auth()->user()->hasRole('super_admin'))
                                            <td>
                                                @if($real_estate->confirm == 0)
                                                    <form action="{{route('dashboard.real_estates_accept', $real_estate->id)}}" method="post" style="display: inline-block">
                                                        @csrf
                                                        @method('put')
                                                        <button type="submit" class="btn btn-danger btn-sm Accept"><i class="fa fa-warning"></i>{{__('site.Accept')}}</button>
                                                    </form>
                                                @elseif($real_estate->confirm == 1)
                                                    <form action="{{route('dashboard.real_estates_unAccept', $real_estate->id)}}" method="post" style="display: inline-block">
                                                        @csrf
                                                        @method('put')
                                                        <button type="submit" class="btn btn-success btn-sm unAccept"><i class="fa fa-arrow-up"></i>{{__('site.unAccept')}}</button>
                                                    </form>
                                                @endif
                                            </td>
                                        @endif
                                        <td>
                                            <div class="dropdown">
                                                <button class="dropbtn">Choices</button>
                                                <div class="dropdown-content">
                                                    {{--show buttom--}}
                                                    @if(auth()->user()->hasPermission('read_real_estates'))
                                                        <a href="{{route('dashboard.real_estates.show', $real_estate->id)}}" class="btn btn-info btn-sm"><i class="fa fa-eye"> {{__('site.show')}}</i></a>
                                                    @else
                                                        <a href="#" disabled="" class="btn btn-info btn-sm"><i class="fa fa-eye">{{__('site.show')}}</i></a>
                                                    @endif

                                                    {{--Edit buttom--}}
                                                    @if(auth()->user()->hasPermission('update_real_estates'))
                                                        <a href="{{route('dashboard.real_estates.edit', $real_estate->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-edit">{{__('site.Edit')}}</i></a>
                                                    @else
                                                        <a href="#" disabled="" class="btn btn-warning btn-sm"><i class="fa fa-edit">{{__('site.Edit')}}</i></a>
                                                    @endif

                                                    {{--Delete buttom--}}
                                                    @if(auth()->user()->hasPermission('delete_real_estates'))
                                                        <form action="{{route('dashboard.real_estates.destroy', $real_estate->id)}}" method="post" style="display: inline-block">
                                                            @csrf
                                                            @method('delete')
                                                            <button type="submit" class="btn btn-danger btn-sm delete b1"><i class="fa fa-trash"></i>{{__('site.Delete')}}</button>
                                                        </form>
                                                    @else
                                                        <a href="#" disabled="" class="btn btn-danger btn-sm b1"><i class="fa fa-edit">{{__('site.Delete')}}</i></a>
                                                    @endif
                                                </div>
                                            </div>


                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            @endif
                        </table>
                        {{$real_estates->appends(request()->query())->links()}}
                    @else
                        <h3 style="font-weight: 400; text-align: center"> No Record Found</h3>
                    @endif
                </div>
            </div>
        @else

            <div class="row">
                <div class="col-md-12">
                    <p class="text-center" style="color: red">  لن يتم قبول اي طلب لعرض المنتج الى بعد قبول العرض من قبل إدارة الموقع - انتظر 24 ساعة حتى يتم مرجعة عرضك</p>
                    <p class="text-center" style="color: red">عندما يظهر عرضك في الجدول أدناه يكون عرضك مقبول وسيعرض في الموقع</p>
                    <hr>
                    @if($real_estates->count() > 0 )
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{__('site.name')}}</th>
                                {{--                            <th>{{__('site.details')}}</th>--}}
                                <th>{{__('site.location')}}</th>
                                <th>{{__('site.price')}}</th>
                                <th>{{__('site.image')}}</th>
                                <th>{{__('site.action')}}</th>
                            </tr>
                            </thead>
                                <tbody>
                                @foreach($real_estates as $index=>$real_estate)
                                    @if($real_estate->confirm == 1 && $real_estate->user_id == auth()->user()->id )
                                        <tr>
                                            <td>{{++$index}}</td>
                                            <td>{{$real_estate->name}}</td>
                                            {{--                                <td> {{\Illuminate\Support\Str::limit($real_estate->details, 50)}} </td>--}}
                                            <td>{{$real_estate->location}}</td>
                                            <td>{{$real_estate->price}} $</td>
                                            <td>
                                                @if($real_estate->image == null)
                                                    <img class="imageSize" src="{{asset('landing_real/images/logo.png')}}" alt="">
                                                @else
                                                    <img class="imageSize" src="{{asset('storage/'.$real_estate->image)}}" alt="">
                                                @endif
                                            </td>
                                            <td>
                                                {{--show buttom--}}
                                                @if(auth()->user()->hasPermission('read_real_estates'))
                                                    <a href="{{route('dashboard.real_estates.show', $real_estate->id)}}" class="btn btn-info btn-sm"><i class="fa fa-eye"> {{__('site.show')}}</i></a>
                                                @else
                                                    <a href="#" disabled="" class="btn btn-info btn-sm"><i class="fa fa-eye">{{__('site.show')}}</i></a>
                                                @endif
                                                {{--Edit buttom--}}
                                                @if(auth()->user()->hasPermission('update_real_estates'))
                                                    <a href="{{route('dashboard.real_estates.edit', $real_estate->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-edit">{{__('site.Edit')}}</i></a>
                                                @else
                                                    <a href="#" disabled="" class="btn btn-warning btn-sm"><i class="fa fa-edit">{{__('site.Edit')}}</i></a>
                                                @endif

                                                {{--Delete buttom--}}
                                                @if(auth()->user()->hasPermission('delete_real_estates'))
                                                    <form action="{{route('dashboard.real_estates.destroy', $real_estate->id)}}" method="post" style="display: inline-block">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i>{{__('site.Delete')}}</button>
                                                    </form>
                                                @else
                                                    <a href="#" disabled="" class="btn btn-danger btn-sm"><i class="fa fa-edit">{{__('site.Delete')}}</i></a>
                                                @endif

                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                        </table>
                        {{$real_estates->appends(request()->query())->links()}}
                    @else
                        <h3 style="font-weight: 400; text-align: center"> No Record Found</h3>
                    @endif
                </div>
            </div>

        @endif

    </div>{{--end-of-tile mb-4--}}


@endsection
