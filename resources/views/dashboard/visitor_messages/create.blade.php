@extends('layouts.dashboard.app')

@section('content')
    <div class="app-title">
        <div>
            @if(isset($visitor_message)  )
                <h1>
                    <i class="fa fa-edit">
                        {{ __('site.Details_visitor_messages')}}
                    </i>
               </h1>
            @else
                <h1>
                    <i class="fa fa-plus">
                      {{__('site.Details_visitor_messages') }}
                    </i>
                </h1>
            @endif

        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-list"></i></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">{{__('site.Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('dashboard.visitor_messages.index')}}">{{__('site.visitor_messages')}}</a></li>
            @if(isset($visitor_message))
                <li class="breadcrumb-item">{{__('site.Details_visitor_messages')}}</li>
            @else
                <li class="breadcrumb-item">{{__('site.Details_visitor_messages')}}</li>
            @endif
        </ul>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-md-12">
             <form action="{{isset($visitor_message)?route('dashboard.visitor_messages.update',$visitor_message->id):route('dashboard.visitor_messages.store')}}" method="post">
                 <div class="timeline-post">
                     <div class="post-media">
                         <div class="content">
                             <h5><a href="#">Name: {{isset($visitor_message)?$visitor_message->name:'no data'}} <br><br> Email: {{isset($visitor_message)?$visitor_message->email:'no data'}} </a></h5>
                             <p class="text-muted"><small>{{isset($visitor_message)?$visitor_message->created_at->diffForHumans():'no data'}}</small></p>
                         </div>
                     </div>
                     <div class="post-content">
                         <p>
                             {{isset($visitor_message)?$visitor_message->messages:'no data'}}
                         </p>
                     </div>
                     <ul class="post-utility">
                         <li class="shares"><a href="#"><i class="fa fa-fw fa-lg fa-share"></i> Phone Number: {{isset($visitor_message)?$visitor_message->phone_number:'no data'}}</a></li>
                     </ul>
                 </div>

                 <div class="form-group">
                     <a class="btn btn-primary" href="{{route('dashboard.visitor_messages.index')}}">
                         <i class="fa fa-backward"></i>
                         {{__('site.back')}}
                     </a>
                 </div>
             </form>

            </div>{{-- end-of-col-12 --}}
        </div>{{--end-of-row--}}


    </div>{{--end-of-tile mb-4--}}


@endsection
