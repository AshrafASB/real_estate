@php
$cont = 0;
@endphp

@extends('layouts.dashboard.app')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                @if(isset($users))
                    <h3 class="tile-title">Show User Details</h3>
                @elseif(isset($UserDetail))
                    <h3 class="tile-title">Update User Details</h3>
                @endif
                <div class="tile-body">
                    <form action="{{isset($UserDetail)?route('dashboard.UserDetails.update',$UserDetail->id):''}}" method="post">
                        @if(isset($UserDetail))
                            @method('put')
                            @csrf
                        @endif
                        <div class="form-group">
                            <label class="control-label">Name</label>
                            @if(isset($UserDetail))
                                <input class="form-control" type="text" name="name"  value="{{$UserDetail->name}}" placeholder="Enter full name">
                            @elseif (isset($users))
                                <input class="form-control" type="text" name="name" disabled value="{{$users->name}}" placeholder="Enter full name">

                            @endif
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            @if(isset($UserDetail))
                                <input class="form-control" type="email"  name="email"  value="{{$UserDetail->email}}" placeholder="Enter email address">
                            @elseif(isset($users))
                                <input class="form-control" type="email"  name="email" disabled value="{{$users->email}}" placeholder="Enter email address">
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="control-label">Password</label>
                            @if(isset($UserDetail))
                                <input class="form-control" type="password"  name="password"   placeholder="Enter password">
                            @elseif(isset($users))
                                <input class="form-control" type="password"  name="password" disabled  placeholder="Enter password">
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="control-label">Confirm Password</label>
                            @if(isset($UserDetail))
                                <input class="form-control" type="password"  name="password_confirmation"   placeholder="Enter password">
                            @elseif(isset($users))
                                <input class="form-control" type="password"  name="password_confirmation" disabled  placeholder="Enter password">
                            @endif

                        </div>
                            <div class="tile-footer">
                                @if(isset($UserDetail))
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;&nbsp;&nbsp;
                                @elseif(isset($users))
                                    <a class="btn btn-primary" href="{{route('dashboard.UserDetails.edit',$users->id)}}"><i class="fa fa-fw fa-lg fa-check-circle"></i>Edit</a>
                                @endif
                            </div>
                    </form>
                </div>

            </div>
        </div>

    </div>


@endsection

