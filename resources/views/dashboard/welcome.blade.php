@extends('layouts.dashboard.app')

@section('content')
{{--    header  --}}
<div class="app-title">
    <div>
        <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
    </div>
    <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
    </ul>
</div>
@if(auth()->user()->hasPermission('read_dashboard'))

<div class="card-deck">
    {{--card 1--}}
    <div class="col-md-6 col-lg-3">
        <div class="widget-small info coloured-icon"><i class="icon fa fa-users fa-3x"></i>
            <div class="info">
                <h4>Users</h4>
                <p><b>{{$users->count()}}</b></p>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-lg-3">
        <div class="widget-small danger coloured-icon"><i class="icon fa fa-star fa-3x"></i>
            <div class="info">
                <h4>Real Estate</h4>
                <p><b>{{$real_estates->count()}}</b></p>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-4">
        <div class="widget-small warning coloured-icon"><i class="icon fa fa-star fa-3x"></i>
            <div class="info">
                <h4>VIP Real Estate</h4>
                <p><b>{{$vip_real_estates->count()}}</b></p>
            </div>
        </div>
    </div>


</div>
<div class="row">
    <div class="col-md-6">
        <div class="tile">
            <h3 class="tile-title">Last Visitor Messages</h3>
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('site.name')}}</th>
                            <th>{{__('site.email')}}</th>
                            <th>{{__('site.phone_number')}}</th>
                            <th>Link for Details</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($visitor_messages as $index=>$visitor_message)
                        <tr>
                            <td>{{++$index}}</td>
                            <td>{{$visitor_message->name}}</td>
                            <td>{{$visitor_message->email}}</td>
                            <td>{{$visitor_message->phone_number}}</td>
                            <td>
                                {{--show buttom--}}
                                @if(auth()->user()->hasPermission('update_visitor_messages'))
                                    <a href="{{route('dashboard.visitor_messages.show', $visitor_message->id)}}" class="btn btn-info btn-sm"><i class="fa fa-edit">{{__('site.show')}}</i></a>
                                @else
                                    <a href="#" disabled="" class="btn btn-warning btn-sm"><i class="fa fa-edit">{{__('site.show')}}</i></a>
                                @endif

                            </td>

                        </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    <div class="col-md-6">
        <div class="tile">
            <h3 class="tile-title">Last Real-Estate added</h3>
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('site.name')}}</th>
                            <th>{{__('site.users')}}</th>
                            <th>{{__('site.price')}}</th>
                            <th>Link for Details</th>

                        </tr>
                        </thead>
                        <tbody>
                            @foreach($real_estates as $index=>$real_estate)
                             <tr>
                                <td>{{++$index}}</td>
                                <td>{{$real_estate->name}}</td>
                                <td>{{$real_estate->user->name}}</td>
                                <td>{{$real_estate->price}} $</td>
                                 <td>
                                     {{--show buttom--}}
                                     @if(auth()->user()->hasPermission('update_real_estates'))
                                         <a href="{{route('dashboard.real_estates.show', $visitor_message->id)}}" class="btn btn-info btn-sm"><i class="fa fa-edit">{{__('site.show')}}</i></a>
                                     @else
                                         <a href="#" disabled="" class="btn btn-warning btn-sm"><i class="fa fa-edit">{{__('site.show')}}</i></a>
                                     @endif

                                 </td>
                             </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
@else
    <div>
        <h1>welcome for your Dashboard</h1>
    </div>
@endif


@endsection
