<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <div class="app-sidebar__user"><img class="app-sidebar__user-avatar"
                    src="{{asset('landing_real/images/logo.png')}}"
                    width="90px"
                    hight="100px"
                    alt="User Image">
        <div>
            <p class="app-sidebar__user-name" style="font-size: small">Hi: {{auth()->user()->name}}</p>
{{--            <p class="app-sidebar__user-designation">{{implode(', ' , auth()->user()->roles->pluck('name')->toArray())}}</p>--}}
        </div>
    </div>
    <ul class="app-menu">
        @if(auth()->user()->hasPermission('read_dashboard'))

        <li>
            <a class="app-menu__item" href="{{route('dashboard.welcome')}}">
                <i class="app-menu__icon fa fa-dashboard"></i>
                <span
                    class="app-menu__label">{{ __('site.Dashboard')}}
                </span>
            </a>
        </li>
        @endif

         <li>
         @if(auth()->user()->hasPermission('read_categories'))
            <li class="treeview">
            <li>
                <a class="app-menu__item" href="{{route('dashboard.categories.index')}}">
                    <i class="app-menu__icon fa fa-circle-o"></i>
                    <span
                        class="app-menu__label"> {{ __('site.Category')}}
                </span>
                </a>
            </li>
         @endif


{{--        @if(auth()->user()->hasPermission('read_roles'))--}}
{{--            <li>--}}
{{--                <a class="app-menu__item " href="{{route('dashboard.roles.index')}}">--}}
{{--                    <i class="app-menu__icon fa fa-anchor"></i>--}}
{{--                    <span--}}
{{--                        class="app-menu__label">{{ __('site.Roles')}}--}}
{{--                </span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        @endif--}}

        @if(auth()->user()->hasPermission('read_real_estates'))
            <li>
                <a class="app-menu__item " href="{{route('dashboard.real_estates.index')}}">
                    <i class="app-menu__icon fa fa-users"></i>
                    <span
                        class="app-menu__label">{{ __('site.real_estates')}}
            </span>
                </a>
            </li>
        @endif

        @if(auth()->user()->hasPermission('read_sales_reservations'))
            <li>
                <a class="app-menu__item " href="{{route('dashboard.sales_reservations.index')}}">
                    <i class="app-menu__icon fa fa-users"></i>
                    <span
                        class="app-menu__label">{{ __('site.sales_reservations')}}
                </span>
                </a>
            </li>
        @endif

        @if(auth()->user()->hasPermission('read_vip_real_estates'))
            <li>
                <a class="app-menu__item " href="{{route('dashboard.vip_real_estates.index')}}">
                    <i class="app-menu__icon fa fa-user-secret"></i>
                    <span
                        class="app-menu__label">{{ __('site.vip_real_estates')}}
        </span>
                </a>
            </li>
        @endif
        @if(auth()->user()->hasPermission('read_v_i_p_buy_reservations'))
            <li>
                <a class="app-menu__item " href="{{route('dashboard.v_i_p_buy_reservations.index')}}">
                    <i class="app-menu__icon fa fa-user-secret"></i>
                    <span
                        class="app-menu__label">{{ __('site.v_i_p_buy_reservations')}}
                    </span>
                </a>
            </li>
        @endif

        @if(auth()->user()->hasPermission('read_users'))
            <li>
                <a class="app-menu__item " href="{{route('dashboard.users.index')}}">
                    <i class="app-menu__icon fa fa-users"></i>
                    <span
                        class="app-menu__label">{{ __('site.Users')}}
                </span>
                </a>
            </li>
        @endif
        @if(auth()->user()->hasPermission('read_visitor_messages'))
            <li class="treeview">
            <li>
                <a class="app-menu__item" href="{{route('dashboard.visitor_messages.index')}}">
                    <i class="app-menu__icon fa fa-users"></i>
                    <span
                        class="app-menu__label"> {{ __('site.visitor_messages')}}
                    </span>
                </a>
            </li>
        @endif

        @if(auth()->user()->hasPermission('read_settings'))
            <li class="treeview">
                <a class="app-menu__item" href="#" data-toggle="treeview">
                    <i class="app-menu__icon fa fa-laptop"></i>
                    <span class="app-menu__label">{{ __('site.Settings')}}</span>
                    <i class="treeview-indicator fa fa-angle-right"></i>
                </a>
                <ul class="treeview-menu">

                    <li>
                        <a class="treeview-item" href="{{route('dashboard.settings.social_links')}}"
                           rel="noopener"><i class="icon fa fa-circle-o"></i> {{ __('site.Social Links')}}
                        </a>
                    </li>
                    <li>
                        <a class="treeview-item" href="{{route('dashboard.settings.about_sites')}}"
                           rel="noopener"><i class="icon fa fa-circle-o"></i> {{ __('site.about_sites')}}
                        </a>
                    </li>
                </ul>
            </li>
        @endif



    </ul>
</aside>
