
@php

    $name_site = setting('name_site');

@endphp
<header class="app-header">
    <a class="app-header__logo" href="{{route('home')}}">{{$name_site}}</a>
    <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar"
                                    aria-label="Hide Sidebar"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">

        <!-- User Menu-->
{{--        <li class="dropdown">--}}
            <a class="app-nav__item" target="_blank" href="{{route('welcome')}}" >
                <i class="fa fa-home fa-lg">  Website </i>
            </a>
{{--        </li>--}}
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i
                    class="fa fa-user fa-lg">  Account </i></a>
            <ul class="dropdown-menu settings-menu dropdown-menu-right">
                <li>
                    <a class="dropdown-item" href="{{route('dashboard.UserDetails.index')}}">
                        <i class="fa fa-user-secret"></i>
                        {{ __('account info') }}
                    </a>
                </li>
                <li>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"><i class="fa fa-sign-out fa-lg"></i>
                        {{ __('Logout') }}
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</header>
<style>
    .imageSize{
        width: 100px;
        height:80px;
    }
</style>
