<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
            'name'=>'admin',
            'email'=>'admin@gmail.com',
            'password'=>bcrypt('123456'),
            'email_verified_at'=>Carbon::now(),
        ]);

//        \App\User::create([
//            'name'=>'ahmad',
//            'email'=>'ahmad@gmail.com',
//            'password'=>bcrypt('123456'),
//            'email_verified_at'=>Carbon::now(),
//        ]);
//        \App\User::create([
//            'name'=>'islam',
//            'email'=>'islam@gmail.com',
//            'password'=>bcrypt('123456'),
//            'email_verified_at'=>Carbon::now(),
//        ]);



        $user->attachRoles(['super_admin']);
//        $user2->attachRoles(['user','admin']);
//        $user3->attachRoles(['user','admin']);



    }
}
