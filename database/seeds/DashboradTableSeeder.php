<?php

use Illuminate\Database\Seeder;

class DashboradTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//-------Category
        \App\Category::create([
            'category_name'=>'شقة سكنية',
        ]);
        \App\Category::create([
            'category_name'=>'فيلا سكنية',
        ]);
        \App\Category::create([
            'category_name'=>'عمارة سكنية',
        ]);

//--------RealEstates
        \App\RealEstates::create([
            'name'=>'شقة العودة',
            'details'=>'شقة العودة السكنية شقة العودة السكنية شقة العودة السكنية',
            'location'=>'الرمال - برج الشروق',
            'price'=>5500,
            'bed_room_number'=>4,
            'living_room_number'=>1,
            'parking_number'=>1,
            'kitchen_number'=>1,
            'user_id'=>1,
            'confirm'=>1,
            'category_id'=>1,
        ]);
        \App\RealEstates::create([
            'name'=>'فيلا ريان',
            'details'=>'فيلا ريان فيلا ريان فيلا ريان فيلا ريان',
            'location'=>'الشيخ عجلين',
            'price'=>210000,
            'bed_room_number'=>8,
            'living_room_number'=>3,
            'parking_number'=>2,
            'kitchen_number'=>4,
            'user_id'=>1,
            'confirm'=>1,
            'category_id'=>2,
        ]);
        \App\RealEstates::create([
            'name'=>'عمارة الهدي',
            'details'=>'عمارة الهدي عمارة الهدي عمارة الهدي',
            'location'=>'جباليا - قليبو',
            'price'=>80000,
            'bed_room_number'=>4,
            'living_room_number'=>2,
            'parking_number'=>1,
            'kitchen_number'=>2,
            'user_id'=>1,
            'category_id'=>3,
        ]);

        \App\SalesReservations::create([
            'buyer_name'=>'test_1',
            'national_id'=>'123456789',
            'email'=>'test111@test111.com',
            'phone'=>'123456',
            'address'=>'address address address',
            'details'=>' اريد ان ألتقي بيكم حتى اوثق عملية الشراء',
            'user_id'=>1,
            'real_estate_id'=>1,

        ]);
        \App\SalesReservations::create([
            'buyer_name'=>'test_222',
            'national_id'=>'985632147',
            'email'=>'test2222@test2222.com',
            'phone'=>'123456',
            'address'=>'address address address',
            'details'=>' اريد ان ألتقي بيكم حتى اوثق عملية الشراء',
            'user_id'=>1,
            'real_estate_id'=>1,

        ]);


        \App\VIP_RealEstate::create([
            'name'=>'VIP Department',
            'details'=>'VIP Department VIP Department VIP Department',
            'location'=>'center city',
            'price'=>80000,
            'user_id'=>1,
            'category_id'=>3,
        ]);
        \App\VIP_RealEstate::create([
            'name'=>'VIP House',
            'details'=>'VIP House VIP House VIP House',
            'location'=>'center city',
            'price'=>120000,
            'user_id'=>1,
            'category_id'=>1,
        ]);
        \App\VIP_RealEstate::create([
            'name'=>'VIP Building',
            'details'=>'VIP Building VIP Building VIP Building',
            'location'=>'center city',
            'price'=>200000,
            'user_id'=>1,
            'category_id'=>2,
        ]);
        \App\VIP_RealEstate::create([
            'name'=>'VIP Upscale sea view',
            'details'=>'VIP Upscale sea view VIP Upscale sea view',
            'location'=>'center city',
            'price'=>200000,
            'user_id'=>1,
            'category_id'=>1,
        ]);
        \App\VIP_RealEstate::create([
            'name'=>'VIP Great hotels',
            'details'=>'VIP Great hotels VIP Great hotels VIP Great hotels',
            'location'=>'center city',
            'price'=>200000,
            'user_id'=>1,
            'category_id'=>1,
        ]);

        \App\VIPBuyReservation::create([
            'buyer_name'=>'test_1111111111111',
            'national_id'=>'985632147',
            'email'=>'test_1111111111111@test2222.com',
            'phone'=>'123456',
            'address'=>'address address address',
            'details'=>' اريد ان ألتقي بيكم حتى اوثق عملية الشراء',
            'user_id'=>1,
            'vip_real_estate_id'=>1,

        ]);
        \App\VIPBuyReservation::create([
            'buyer_name'=>'test_222222222',
            'national_id'=>'985632147',
            'email'=>'test_222222222@test2222.com',
            'phone'=>'123456',
            'address'=>'address address address',
            'details'=>' اريد ان ألتقي بيكم حتى اوثق عملية الشراء',
            'user_id'=>1,
            'vip_real_estate_id'=>2,

        ]);
        \App\VIPBuyReservation::create([
            'buyer_name'=>'test_333333333',
            'national_id'=>'985632147',
            'email'=>'test_333333332@test2222.com',
            'phone'=>'123456',
            'address'=>'address address address',
            'details'=>' اريد ان ألتقي بيكم حتى اوثق عملية الشراء',
            'user_id'=>1,
            'vip_real_estate_id'=>2,

        ]);


        \App\VisitorMessages::create([
            'name'=>'Ahmad Ali',
            'email'=>'Ahmad@gmail.com',
            'phone_number'=>'059124578',
            'messages'=>'Ahmad Ali Ahmad Ali Ahmad Ali Ahmad Ali Ahmad Ali Ahmad Ali Ahmad Ali Ahmad Ali Ahmad Ali Ahmad Ali Ahmad Ali ',

        ]);

    }
}
