<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVIPBuyReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('v_i_p_buy_reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('buyer_name');
            $table->string('national_id');
            $table->string('email');
            $table->string('phone');
            $table->string('address');
            $table->text('details')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('vip_real_estate_id');
            $table->timestamps();


            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('vip_real_estate_id')->references('id')->on('real_estates')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v_i_p_buy_reservations');
    }
}
