<?php

use Illuminate\Support\Facades\Route;

Route::prefix('dashboard')
    ->name('dashboard.')
//    ->middleware(['auth'])
//    ->middleware(['auth','role:super_admin,admin'])
    ->group(function (){

    //dashboard.welcome  - welcome_Route
        Route::get('/','welcomeController@index')->name('welcome');

    //category Routes
        Route::resource('categories','CategoryController')->except(['show']);

    //sub Categories
//        Route::resource('sub_categories','SubCategoryController')->except(['show']);

    //contact_us
            Route::resource('contact_us','ContactUsController')->except(['show']);

    //Role Route
        Route::resource('roles','RoleController');

    //User Route
        Route::resource('users','UserController');

    //UserDetails Route
        Route::resource('UserDetails','UserDetails');

    //RealEstates Route
        Route::resource('real_estates','RealEstatesController');
        Route::put('real_estates_accept/{real_estate}','RealEstatesController@Accept')->name('real_estates_accept');
        Route::put('real_estates_unAccept/{real_estate}','RealEstatesController@unAccept')->name('real_estates_unAccept');


        //RequestBuysOrders Route
        Route::resource('request_buys_orders','RequestBuysOrdersController');

        //vip_real_estates Route
        Route::resource('vip_real_estates','VIP_RealEstateController');

        //v_i_p_buy_reservations Route
        Route::resource('v_i_p_buy_reservations','VIPBuyReservationController');

    //SalesReservations Route
        Route::resource('sales_reservations','SalesReservationsController');
        //visitor_messages Route
        Route::resource('visitor_messages','VisitorMessagesController');

        // Setting Route
        Route::get('/settings/social_login','SettingController@Social_Login')->name('settings.social_login');
        Route::get('/settings/social_link','SettingController@Social_Links')->name('settings.social_links');
        Route::post('/social_links','SettingController@store')->name('settings.store');
        //about_sites
        Route::get('/settings/about_sites','SettingController@about_sites')->name('settings.about_sites');
        Route::post('/about_sites','SettingController@store_about_sites')->name('settings.store_about_sites');

    });
