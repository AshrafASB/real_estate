<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::get('/about', function () {
    return view('website.about');
})->name('about');
Route::get('/contact', function () {
    return view('website.contact');
})->name('contact');

Auth::routes();

Route::get('/', 'LandingPageController@Landing')->name('welcome');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/storeMessage', 'LandingPageController@storeMessage')->name('storeMessage');

Route::post('/buy_real_estate', 'LandingPageController@buy_real_estate')->name('buy_real_estate');
Route::get('/property_detail/{id}', 'LandingPageController@property_detail')->name('property_detail');

Route::get('/reservation_vip/{id}', 'LandingPageController@reservation_vip')->name('reservation_vip');
Route::post('/v_i_p_buy_reservations', 'LandingPageController@v_i_p_buy_reservations')->name('v_i_p_buy_reservations');

Route::get('/all_real_estate', 'LandingPageController@all_real_estate')->name('all_real_estate');


//Route::post('consultation_requests','App\Http\Controllers\Dashboard\Consultation_requestsController@store')->name('consultation_requests');

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider')->where('provider','facebook|google|youtube');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->where('provider','facebook|google|youtube');

