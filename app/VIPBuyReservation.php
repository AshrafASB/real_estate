<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VIPBuyReservation extends Model
{
    protected $guarded = [];
    protected $table = 'v_i_p_buy_reservations';

    //Relation -----------------------------------------
    public function vip_real_estate(){
        return $this->belongsTo(VIP_RealEstate::class,'vip_real_estate_id','id');
    }//end of service

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }//end of service


    //Attribute -----------------------------------------
    /* To make any value in name column as UpperCase  */
    public function getNameAttribute ($value){
        return ucfirst($value);
    }//end of getAttribute

    //Scope ----------------------------------------------
    /* I use scope for i can call it easy from controller == so I can use it in controller as WhenSearch without scope */
    public function scopeWhenSearch($query , $search){
        return $query->when($search ,function ($q) use ($search){
            return $q->where('name','like',"%$search%");
        });

    }//end of scopeWhenSearch
}
