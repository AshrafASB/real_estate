<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Http\Controllers\Controller;
use App\User;
use App\VIP_RealEstate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VIP_RealEstateController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.vip_real_estates.";

        //Permissions
        $this->middleware('permission:read_real_estates')->only(['index']);
        $this->middleware('permission:create_real_estates')->only(['create','store']);
        $this->middleware('permission:update_real_estates')->only(['edit','update']);
        $this->middleware('permission:delete_real_estates')->only(['destroy']);

    }

    public function index()
    {
        $vip_real_estates = VIP_RealEstate::WhenSearch(request()->search)->paginate(8);
        return view($this->path.'index',compact('vip_real_estates'));
    }//end of index
    public function create()
    {
        $users = User::all();
        $categories = Category::all();
        return view($this->path.'create',compact(['users','categories']));
    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:v_i_p__real_estates,name',
        ]);
        $data = $request->except(['image']);
        if ($request->hasFile('image')){
            $image = $request->image->store('images','public');
            $data['image'] = $image;
        }
        if (!auth()->user()->hasRole('super_admin')){
            $data['user_id'] = auth()->user()->id;
        }
        VIP_RealEstate::create($data);
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show(VIP_RealEstate $vip_real_estate)
    {
        $users = User::all();
        $categories = Category::all();
        return view($this->path.'show',compact(['vip_real_estate','users','categories']));

    }//end of show

    public function edit(VIP_RealEstate $vip_real_estate)
    {
        $users = User::all();
        $categories = Category::all();
        return view($this->path.'create',compact(['vip_real_estate','users','categories']));
    }//end of edit

    public function update(Request $request, VIP_RealEstate $vip_real_estate)
    {
        $request->validate([
            'name' => 'required|unique:v_i_p__real_estates,name,'.$vip_real_estate->id,
        ]);
        $data = $request->except(['image']);

        if ($request->hasFile('image')){
            $image = $request->image->store('images','public');
            Storage::disk('public')->delete($vip_real_estate->image);
            $data['image'] = $image;
        }
        if (!auth()->user()->hasRole('super_admin')){
            $data['user_id'] = auth()->user()->id;
        }
        $vip_real_estate->update($data);

        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(VIP_RealEstate $vip_real_estate)
    {
        $vip_real_estate->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy

}
