<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Http\Controllers\Controller;
use App\RealEstates;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RealEstatesController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.real_estates.";

        //Permissions
        $this->middleware('permission:read_real_estates')->only(['index']);
        $this->middleware('permission:create_real_estates')->only(['create','store']);
        $this->middleware('permission:update_real_estates')->only(['edit','update']);
        $this->middleware('permission:delete_real_estates')->only(['destroy']);

    }

    public function index()
    {
        $real_estates = RealEstates::WhenSearch(request()->search)->paginate(8);
        return view($this->path.'index',compact('real_estates'));
    }//end of index

    public function create()
    {
        $users = User::all()->except(1);
        $categories = Category::all();
        return view($this->path.'create',compact(['users','categories']));
    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:real_estates,name',
        ]);
        $data = $request->except(['image','image2','image3','image4','user_id']);
        if ($request->hasFile('image')){
            $image = $request->image->store('images','public');
            $data['image'] = $image;
        }

        if ($request->hasFile('image2')){
            $image2 = $request->image2->store('images','public');
            $data['image2'] = $image2;
        }

        if ($request->hasFile('image3')){
            $image3 = $request->image3->store('images','public');
            $data['image3'] = $image3;
        }

        if ($request->hasFile('image4')){
            $image4 = $request->image4->store('images','public');
            $data['image4'] = $image4;
        }
        if (!auth()->user()->hasRole('super_admin')){
            $data['user_id'] = auth()->user()->id;
        }

        RealEstates::create($data);


        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show(RealEstates $real_estate)
    {
        $users = User::all();
        $categories = Category::all();
        return view($this->path.'show',compact(['real_estate','users','categories']));

    }//end of show

    public function edit(RealEstates $real_estate)
    {
        $users = User::all();
        $categories = Category::all();
        return view($this->path.'create',compact(['real_estate','users','categories']));
    }//end of edit

    public function update(Request $request, RealEstates $real_estate)
    {
        $request->validate([
            'name' => 'required|unique:real_estates,name,'.$real_estate->id,
        ]);
        $data = $request->except(['image','image2','image3','image4']);

        if ($request->hasFile('image')){
            $image = $request->image->store('images','public');
            Storage::disk('public')->delete($real_estate->image);
            $data['image'] = $image;
        }

        if ($request->hasFile('image2')){
            $image2 = $request->image2->store('images','public');
            Storage::disk('public')->delete($real_estate->image2);
            $data['image2'] = $image2;
        }

        if ($request->hasFile('image3')){
            $image3 = $request->image3->store('images','public');
            Storage::disk('public')->delete($real_estate->image3);
            $data['image3'] = $image3;
        }

        if ($request->hasFile('image4')){
            $image4 = $request->image4->store('images','public');
            Storage::disk('public')->delete($real_estate->image4);
            $data['image4'] = $image4;
        }
        if (!auth()->user()->hasRole('super_admin')){
            $data['user_id'] = auth()->user()->id;
        }
        $real_estate->update($data);

        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(RealEstates $real_estate)
    {
        $real_estate->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy


    public function Accept(RealEstates $real_estate){
        $real_estate->update([
            'confirm'=>1
        ]);
        session()->flash('success',__('site.Request Accepted'));
        return redirect()->route($this->path.'index');
    }//end of

    public function unAccept(RealEstates $real_estate){

        $real_estate->update([
            'confirm'=>0
        ]);
        session()->flash('success',__('site.Request UnAccepted'));
        return redirect()->route($this->path.'index');
    }//end of
}
