<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\RealEstates;
use App\SalesReservations;
use App\User;
use Illuminate\Http\Request;

class SalesReservationsController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.sales_reservations.";

        //Permissions
        $this->middleware('permission:read_sales_reservations')->only(['index']);
        $this->middleware('permission:create_sales_reservations')->only(['create','store']);
        $this->middleware('permission:update_sales_reservations')->only(['edit','update']);
        $this->middleware('permission:delete_sales_reservations')->only(['destroy']);

    }

    public function index()
    {
        $sales_reservations = SalesReservations::WhenSearch(request()->search)
            ->with(['real_estate','user'])
            ->paginate(5);
        return view($this->path.'index',compact('sales_reservations'));
    }//end of index

    public function create()
    {
        $users = User::all();
        $real_estates = RealEstates::all();

        return view($this->path.'create',compact(['users','real_estates']));
    }//end of create

    public function store(Request $request)
    {
//        $request->validate([
//            'category_name' => 'required|unique:sales_reservations,category_name',
//        ]);
        SalesReservations::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show(SalesReservations $sales_reservation)
    {
        $users = User::all();
        $real_estates = RealEstates::all();
        return view($this->path.'show',compact(['users','real_estates','sales_reservation']));
    }//end of show

    public function edit(SalesReservations $sales_reservation)
    {
        $users = User::all();
        $real_estates = RealEstates::all();
        return view($this->path.'create',compact(['users','real_estates','sales_reservation']));
    }//end of edit

    public function update(Request $request, SalesReservations $sales_reservation)
    {
//        $request->validate([
//            'sales_reservation_name' => 'required|unique:sales_reservations,sales_reservation_name,'.$sales_reservation->id,
//        ]);
        $sales_reservation->update($request->all());
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(SalesReservations $sales_reservation)
    {
        $sales_reservation->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
