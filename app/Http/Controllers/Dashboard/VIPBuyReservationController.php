<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\RealEstates;
use App\VIP_RealEstate;
use App\VIPBuyReservation;
use App\User;
use Illuminate\Http\Request;

class VIPBuyReservationController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.v_i_p_buy_reservations.";

        //Permissions
        $this->middleware('permission:read_v_i_p_buy_reservations')->only(['index']);
        $this->middleware('permission:create_v_i_p_buy_reservations')->only(['create','store']);
        $this->middleware('permission:update_v_i_p_buy_reservations')->only(['edit','update']);
        $this->middleware('permission:delete_v_i_p_buy_reservations')->only(['destroy']);

    }

    public function index()
    {
        $v_i_p_buy_reservations = VIPBuyReservation::WhenSearch(request()->search)
            ->with(['vip_real_estate','user'])
            ->paginate(5);
        return view($this->path.'index',compact('v_i_p_buy_reservations'));
    }//end of index

    public function create()
    {
        $users = User::all();
        $real_estates = RealEstates::all();

        return view($this->path.'create',compact(['users','real_estates']));
    }//end of create

    public function store(Request $request)
    {
        VIPBuyReservation::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of store

    public function show(VIPBuyReservation $v_i_p_buy_reservation)
    {
        $users = User::all();
        $vip_real_estates = VIP_RealEstate::all();
        return view($this->path.'show',compact(['users','vip_real_estates','v_i_p_buy_reservation']));
    }//end of show

    public function edit(VIPBuyReservation $v_i_p_buy_reservation)
    {
        $users = User::all();
        $vip_real_estates = VIP_RealEstate::all();
        return view($this->path.'create',compact(['users','vip_real_estates','v_i_p_buy_reservation']));
    }//end of edit

    public function update(Request $request, VIPBuyReservation $v_i_p_buy_reservation)
    {

        $v_i_p_buy_reservation->update($request->all());
        session()->flash('success',__('site.DataUpdatedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of update

    public function destroy(VIPBuyReservation $v_i_p_buy_reservation)
    {
        $v_i_p_buy_reservation->delete();
        session()->flash('success',__('site.DataDeletedSuccessfully'));
        return redirect()->route($this->path.'index');
    }//end of destroy
}
