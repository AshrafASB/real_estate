<?php

namespace App\Http\Controllers\Dashboard;

use App\Advertisement;
use App\AdvertisementItems;
use App\BloodDonation;
use App\Category;
use App\Consultation_requests;
use App\ContactUs;
use App\Http\Controllers\Controller;
use App\RealEstates;
use App\ServiceItem;
use App\User;
use App\VIP_RealEstate;
use App\VisitorMessages;
use App\WhoAreWe;
use Carbon\Carbon;
use Illuminate\Http\Request;

class welcomeController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "dashboard.welcome";

        //Permissions
        $this->middleware('permission:read_dashboard')->only(['index']);
        $this->middleware('permission:create_dashboard')->only(['create','store']);
        $this->middleware('permission:update_dashboard')->only(['edit','update']);
        $this->middleware('permission:delete_dashboard')->only(['destroy']);
    }

    public function index (){
        $users= User::all();
        $visitor_messages = VisitorMessages::WhenSearch(request()->search)->paginate(10);
        $real_estates = RealEstates::WhenSearch(request()->search)->paginate(10);
        $vip_real_estates = VIP_RealEstate::WhenSearch(request()->search)->paginate(10);
        return view($this->path,compact(['visitor_messages','real_estates','vip_real_estates','users']));
    }//end of function





}
