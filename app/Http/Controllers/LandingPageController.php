<?php

namespace App\Http\Controllers;


use App\Category;
use App\ContactUs;
use App\RealEstates;
use App\SalesReservations;
use App\TeamWork;
use App\VIP_RealEstate;
use App\VIPBuyReservation;
use App\VisitorMessages;
use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    public function __construct()
    {
        //Parent Path
        $this->path = "website.index";
    }

    public function Landing(){
        $real_estates = RealEstates::all();
        $vip_real_estates = VIP_RealEstate::all();
        return view($this->path,compact(['real_estates','vip_real_estates']));

    }//end of Landing


    public function storeMessage(Request $request)
    {
        VisitorMessages::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return view('website.thanks');
    }//end of storeMessage

    public function buy_real_estate(Request $request)
    {
        SalesReservations::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return view('website.receive_reservation');
    }//end of store


    public function property_detail (RealEstates $id){
        $property_detail = $id;
        $vip_real_estates = VIP_RealEstate::all();
        return view('website.property_detail',compact(['property_detail','vip_real_estates']));

    }//end of property_detail

    public function reservation_vip (VIP_RealEstate $id){
        $reservation_vip = $id;
        return view('website.reservation_vip',compact(['reservation_vip','vip_real_estates']));

    }//end of reservation_vip

    public function v_i_p_buy_reservations(Request $request)
    {
            VIPBuyReservation::create($request->all());
        session()->flash('success',__('site.DataAddSuccessfully'));
        return view('website.receive_reservation');
    }//end of store



    public function all_real_estate (){
        $real_estates = RealEstates::WhenSearch(request()->search)->paginate(12);
        $vip_real_estates = VIP_RealEstate::all();
        return view('website.all_real_estate',compact(['real_estates','vip_real_estates']));

    }//end of all_real_estate




}
